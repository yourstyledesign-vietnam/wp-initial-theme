<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for Archive page
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>
<?php get_header(); ?>
<div class="container">

  <div class="navbar navbar-default navbar-static navbar-filter">   
    <div class="navbar-header">
      <span class="navbar-brand">
        <i class="ion-funnel"></i>
      </span>
    </div>
    <ul class="nav navbar-nav navbar-category">        
      <li class="dropdown">
          <a data-target="dropdown" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            Category
            <span class="caret"></span>
          </a>
        <ul class="dropdown-menu" aria-labelledby="dLabel">
          <?php $args = array( 'hide_empty' => false, 'title_li' => false ); wp_list_categories($args); ?>
        </ul>
      </li> 
    </ul>  
    <ul class="nav navbar-nav navbar-right">
      <?php $args = array( 'hide_empty' => false, 'title_li' => false ); wp_list_categories($args); ?>
    </ul>
  </div>

      <?php if (have_posts()) : ?>
      
        <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
        <?php /* If this is a category archive */ if (is_category()) { ?>
          <header class="archive-page-title">Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</header>
        <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
        <header class="archive-page-title">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</header>
        <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
        <header class="archive-page-title">Archive for <?php the_time('F jS, Y'); ?></header>
        <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
        <header class="archive-page-title">Archive for <?php the_time('F, Y'); ?></header>
        <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
        <header class="archive-page-title">Archive for <?php the_time('Y'); ?></header>
        <?php /* If this is an author archive */ } elseif (is_author()) { ?>
        <header class="archive-page-title">Author Archive</header>
        <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
        <header class="archive-page-title">Blog Archives</header>
        <?php } ?>

        <div class="cards">
          <?php           
            while (have_posts()) : the_post(); 
              include(locate_template( 'component/card-block.php' ));
            endwhile;
          ?>
        </div>

        <?php include(locate_template('component/wp-pager.php')); ?>
      <?php endif; ?>

</div>
<?php get_footer(); ?>
