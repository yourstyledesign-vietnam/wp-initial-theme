<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for 404 page
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>
<?php get_header(); ?>
  <div class="container">
    <div class="fourzerofour">
    	<?php if (ot_get_option('page404_image')): ?>
    	<div class="page404-image">
    		<?php echo ot_get_option('page404_image'); ?>        	
    	</div>
    	<?php endif ?>
        <h1>
			<?php echo ot_get_option('page404_title'); ?>        	
        </h1>     
        <p><?php echo ot_get_option('page404_content'); ?></p><br /> 
        <a class="btn btn-primary" href="<?php echo get_option('home'); ?>/"><i class="ion-arrow-left-c"></i> <?php _e("Back to home","mystyle" ); ?></a>
    </div>
  </div>
<?php get_footer(); ?>