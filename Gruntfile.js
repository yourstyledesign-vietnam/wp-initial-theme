module.exports = function(grunt) {
 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            dist: {
                src: 'readme.txt',
                dest: 'README.md'
            }
        },
        phpdocumentor: {
            dist: {
                options: {
                    directory : './',
                    target : 'docs',
                    ignore: 'node_modules'
                }
            }
        },              
    });

    grunt.loadNpmTasks('grunt-contrib-copy');  
    grunt.loadNpmTasks('grunt-contrib-uglify');    
    grunt.loadNpmTasks('grunt-phpdocumentor');
    

    grunt.registerTask('default', [               
        'copy'
    ]);
 
    grunt.registerTask('docs', [
        'phpdocumentor:dist'
    ]);
};