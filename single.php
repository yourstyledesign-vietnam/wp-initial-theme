<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for single post
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>
<?php get_header(); ?>
<div class="container">
  <div class="single-post-container">
  
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
        <header>
          <h1 class="single-post-title"><?php the_title(); ?>  </h1>        
        </header>
        <?php include(locate_template('component/post-meta.php' )); ?>
        <?php include(locate_template('component/share-social.php' )); ?>

        <div class="blog-post">
          <div class="post-content">
            <?php the_content(); ?>
          </div> <!-- post-content -->   
        </div>
      </article><!-- single-post-container -->
      <?php include(locate_template('component/read-next.php' )); ?>
    <?php endwhile; endif; ?>
   
  </div>
</div> <!-- container -->
<?php get_footer(); ?>
