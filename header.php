<?php
/**
 * Template header
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.0
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">    
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />    
    
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <?php if (ot_get_option('site_favicon')): ?>
      <link href="<?php echo ot_get_option('site_favicon'); ?>" sizes="32x32" rel="icon" type="image/png" />  
    <?php endif; ?>


    <?php if (ot_get_option('iphone_icon_retina')): ?>
      <link rel="icon" href="<?php echo ot_get_option('iphone_icon_retina'); ?>" sizes="180x180" type="image/png"  />        
    <?php endif; ?>   
    
    <link rel="canonical" href="<?php the_permalink(); ?>" />
    
    <!-- Wordpress Head Items -->
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    
    <?php wp_head(); ?>

</head>
<body <?php body_class('mystyle'); ?>>
  <!--[if lt IE 8]>
    <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
  <![endif]-->
  
  <?php   
    if ( ot_get_option('toolbar_on_off') == 'on' ) : 
      include(locate_template('component/toolbar.php'));
    endif; 
  ?>  

  <!-- HEADER -->
  <div id="header">
    <header class="header">
    <div class="header-inner">
      <div class="navbar navbar-default main-nav-wrap">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo get_option('home'); ?>/">
              <?php  
                $logo_type = ot_get_option('logo_type');
                $logo_text = ot_get_option('logo_text');
                $logo_image = ot_get_option('logo_image');
                $logo_font = ot_get_option('logo_typography');
              ?>
              <?php if ( ($logo_type == 'image' || $logo_type == 'text_image') && !empty($logo_image) ): ?>
                <span class="logo-image">
                  <img src="<?php echo $logo_image; ?>" alt="<?php bloginfo('name'); ?>">  
                </span>
              <?php endif; ?>

              <?php if ( ($logo_type == 'text' || $logo_type == 'text_image') && !empty($logo_text) ): ?>                
                <span class="logo-text">
                <?php echo $logo_text; ?>
                </span>
              <?php endif; ?>
            </a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <!-- MENU -->
            <?php 
              if ( has_nav_menu( 'primary-menu' ) ) {
                $args = array(
                  'theme_location' => 'primary-menu',
                  'container'      => false,
                  'menu'           => 'primary-menu',                      
                  'depth'          => 2,
                  'menu_class'     => 'nav navbar-nav primary-menu',
                  'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
                  'walker'         => new wp_bootstrap_navwalker()
                );
                wp_nav_menu( $args );
              };  
            ?>  
            <?php 
              if (ot_get_option('search_on_off') == 'on') {
                get_search_form(); 
              }
            ?>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </div><!-- navbar-default --> 
    </div>
    </header>
  </div>
  <!-- / HEADER -->

<!-- END of MENU -->

<main id="master" class="master">
  <div class="master-inner">
  
    