<?php 
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for index
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>
<?php get_header(); ?>

<div class="container">
  <?php get_sidebar(); ?>
  <div id="main" role="main">
    <?php if (have_posts()) : ?>
      <?php 
        do_action( 'before_blog_post' );
        while (have_posts()) : the_post(); 
          get_template_part('content','post');
        endwhile; 
        do_action( 'after_blog_post' );
        mystyle_pager();
        //include(locate_template('component/wp-pager.php'));
      ?>
      
    <?php endif; ?>
  </div>
</div>
<?php get_footer(); ?>