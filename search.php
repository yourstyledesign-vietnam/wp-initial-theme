<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for Search Page
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
global $wp_query;
$total_results = $wp_query->found_posts;

?>
<?php get_header(); ?>  
<div class="container">
  <div class="row">

    <!-- ==== SEARCH POSTS ==== -->
    <div class="col-md-9">
      <div id="main" role="main">
      <?php if (have_posts()) : ?>
        <header class="search-title page-title archive-page-title">
          <?php _e( sprintf("Your search for '%s'",get_search_query() ) ); ?>
          <span>
            <?php _e( sprintf("%s results", $total_results ) );  ?>
          </span>
        </header>
        <div class="search-posts">
          <?php 
            do_action( 'before_blog_post' );
            while (have_posts()) : the_post();  
              get_template_part("content","post");
            endwhile; 
            do_action( 'after_blog_post' );
            include(locate_template('component/wp-pager.php'));
          ?>
        </div><!-- search-posts -->
      <?php else : ?>
        <div class="no-post-found">
          <?php _e('No posts found. Try a different search?' ); ?>    
        </div>
      <?php endif; ?>
      </div> <!-- main -->
    </div> <!-- col -->
    <!-- /==== SEARCH POSTS ==== -->

    <!-- ==== SIDEBAR ==== -->
    <div class="col-md-3">
      <div class="primary-sidebar">
        <?php dynamic_sidebar('primary'); ?>
      </div> <!-- primary-sidebar -->
    </div> <!-- col -->
    <!-- / ==== SIDEBAR ==== -->

  </div> <!-- row -->
</div>
<?php get_footer(); ?>
