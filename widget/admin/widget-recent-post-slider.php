<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>
<?php
/*---------------------------------------------------------------------------------*/
/* Recent Posts Slider */
/*---------------------------------------------------------------------------------*/
class Mystyle_Recent_Posts_Slider extends WP_Widget {
			
	function __construct() {
    	$widget_ops = array(
			'classname'   => 'widget_recent_posts_slider', 
			'description' => __('Display recent posts from a category in slider layout',TEMPLATE_NAME)
		);
		$control_options = array(
			'width'  => 750,
			'height' => 350
		);
		parent::__construct( false, __( 'MyStyle :: Recent posts slider', 'mystyle' ), $widget_ops, $control_options );    	
	}

	function widget($args, $instance) {
           
			extract( $args );		
			$title = apply_filters( 'widget_title', empty($instance['title']) ? __( 'MyStyle :: Recent posts slider', 'mystyle' ) : $instance['title'], $instance, $this->id_base);
			
			echo $before_widget;
			// Widget title
			echo $before_title;
			echo $instance["title"];		
			echo $after_title;
			include( locate_template('widget/frontend/widget-recent-post-slider-front.php') );
			echo $after_widget;
	}
	
	function update( $new,$old ) {
		$instance                      = $old;
		$instance['title']             = strip_tags($new['title']);
		// Posts		
		$instance['dotsv']             = $new['dotsv']?1:0;
		$instance['autoplayv']         = $new['autoplayv']?1:0;
		$instance['arrowsv']           = $new['arrowsv']?1:0;
		$instance['speedv']            = $new['speedv'];
		$instance['effect']            = $new['effect'];
		$instance['autoplayIntervalv'] = $new['autoplayIntervalv'];
		$instance['slidesPerView']     = $new['slidesPerView'];
		$instance['slidesPerColumn']     = $new['slidesPerColumn'];
		$instance['posts_num']         = strip_tags($new['posts_num']);
		$instance['layout']            = strip_tags($new['layout']);
		$instance['posts_cat_id']      = strip_tags($new['posts_cat_id']);
		$instance['posts_orderby']     = strip_tags($new['posts_orderby']);
		$instance['posts_time']        = strip_tags($new['posts_time']);
        return $instance;
	}
	
	function form( $instance ) {
		// Default widget settings
		$defaults = array(
			'title'             => '',			
			'layout'            => 1,
			'dotsv'             => 1,
			'autoplayv'         => 1,
			'arrowsv'           => 1,
			'effect'           => 'slide',
			'posts_num'         => '4',
			'speedv'            => 500,
			'autoplayIntervalv' => 500,
			'posts_cat_id'      => '0',
			'posts_orderby'     => 'date',
			'posts_time'        => '0',
			'slidesPerView'     => 1,
			'slidesPerColumn'     => 1,
		);
		$instance = wp_parse_args( (array) $instance, $defaults );		
?>
	<div class="widget-admin-dev">
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php _e("Title:",TEMPLATE_NAME); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $instance["title"] ); ?>" />
		</p>
		<div class="clearfix">
			<div class="widget-col-1">				
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id("posts_cat_id") ); ?>"><?php _e("Category:",TEMPLATE_NAME); ?></label>
					<br>
					<?php wp_dropdown_categories( array( 'name' => $this->get_field_name("posts_cat_id"), 'selected' => $instance["posts_cat_id"], 'show_option_all' => 'All', 'show_count' => true ) ); ?>		
				</p>
				<p style="padding-top: 0.3em;">
					<label for="<?php echo esc_attr( $this->get_field_id("posts_orderby") ); ?>"><?php _e("Order by:",TEMPLATE_NAME ); ?></label>
					<br>
					<select style="width: 100%;" id="<?php echo esc_attr( $this->get_field_id("posts_orderby") ); ?>" name="<?php echo esc_attr( $this->get_field_name("posts_orderby") ); ?>">
					  <option value="date"<?php selected( $instance["posts_orderby"], "date" ); ?>><?php _e("Most recent",TEMPLATE_NAME); ?></option>
					  <option value="comment_count"<?php selected( $instance["posts_orderby"], "comment_count" ); ?>><?php _e("Most commented",TEMPLATE_NAME); ?></option>
					  <option value="rand"<?php selected( $instance["posts_orderby"], "rand" ); ?>><?php _e("Random",TEMPLATE_NAME); ?></option>
					</select>	
				</p>				
			</div>
			<div class="widget-col-2">
				<p>
					<label for="<?php echo esc_attr( $this->get_field_id("posts_num") ); ?>"><?php _e("Items to show",TEMPLATE_NAME ); ?></label>
					<br>
					<select style="width: 100%;" id="<?php echo esc_attr( $this->get_field_id("posts_num") ); ?>" name="<?php echo esc_attr( $this->get_field_name("posts_num") ); ?>">
						<?php for ($i = 2; $i < 10 ; $i++) { 
							echo printf('<option value="%s" %s>%s</option>', $i, selected( $instance["posts_num"], $i ), $i );
						} ?>					  
					</select>	
				</p>				
			</div><!-- col -->
		</div>		
		<hr>
		<label><strong><?php _e('Slider options'); ?></strong></label>
		<div class="clearfix">
			<div class="widget-col-1">

				<p>
					<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('dotsv') ); ?>" name="<?php echo esc_attr( $this->get_field_name('dotsv') ); ?>" <?php checked( (bool) $instance["dotsv"], true ); ?>>
					<label for="<?php echo esc_attr( $this->get_field_id('dotsv') ); ?>"><?php _e("Show dot indicators",TEMPLATE_NAME); ?></label>
				</p>
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('arrowsv') ); ?>" name="<?php echo esc_attr( $this->get_field_name('arrowsv') ); ?>" <?php checked( (bool) $instance["arrowsv"], true ); ?>>
					<label for="<?php echo esc_attr( $this->get_field_id('arrowsv') ); ?>"><?php _e("Show arrows (Prev/Next Arrows)",TEMPLATE_NAME); ?></label>
				</p>
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('autoplayv') ); ?>" name="<?php echo esc_attr( $this->get_field_name('autoplayv') ); ?>" <?php checked( (bool) $instance["autoplayv"], true ); ?>>
					<label for="<?php echo esc_attr( $this->get_field_id('autoplayv') ); ?>"><?php _e("Enables Autoplay",TEMPLATE_NAME); ?></label>
				</p>
			</div>
			<div class="widget-col-2">
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("slidesPerColumn") ); ?>">
					<?php _e("Number of slides per column, for multirow layout",TEMPLATE_NAME ); ?>
						
					</label>
					<input style="width:40%;" id="<?php echo esc_attr( $this->get_field_id("slidesPerColumn") ); ?>" name="<?php echo esc_attr( $this->get_field_name("slidesPerColumn") ); ?>" type="number" value="<?php echo absint($instance["slidesPerColumn"]); ?>" size='1' min="1" max="7" step="1" />
				</p>
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("slidesPerView") ); ?>">
					<?php _e("Number of slides per view",TEMPLATE_NAME ); ?>
						
					</label>
					<input style="width:40%;" id="<?php echo esc_attr( $this->get_field_id("slidesPerView") ); ?>" name="<?php echo esc_attr( $this->get_field_name("slidesPerView") ); ?>" type="number" value="<?php echo absint($instance["slidesPerView"]); ?>" size='1' min="1" max="7" step="1" />
				</p>
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("speedv") ); ?>">
						<?php _e("Speed",TEMPLATE_NAME ); ?>
					</label>
					<input style="width:40%;" id="<?php echo esc_attr( $this->get_field_id("speedv") ); ?>" name="<?php echo esc_attr( $this->get_field_name("speedv") ); ?>" type="number" value="<?php echo absint($instance["speedv"]); ?>" size='6' min="300" max="2000" step="100" />
				</p>
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("autoplayIntervalv") ); ?>"><?php _e("Autoplay Speed (milliseconds)",TEMPLATE_NAME ); ?></label>
					<input style="width:40%;" id="<?php echo esc_attr( $this->get_field_id("autoplayIntervalv") ); ?>" name="<?php echo esc_attr( $this->get_field_name("autoplayIntervalv") ); ?>" type="number" value="<?php echo absint($instance["autoplayIntervalv"]); ?>" size='6' min="300" max="2000" step="100" />
				</p>

				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo esc_attr( $this->get_field_id("effect") ); ?>"><?php _e("Effect",TEMPLATE_NAME ); ?></label>
					
					<?php  
						$effects = array(
							'slide'     => 'Slide',
							'fade'      => 'Fade',
							'cube'      => 'Cube',
							'coverflow' => 'Coverflow',
							'flip'      => 'Flip'
						);
					?>
					<select style="width:40%;" id="<?php echo esc_attr( $this->get_field_id("effect") ); ?>" name="<?php echo esc_attr( $this->get_field_name("effect") ); ?>">
						<?php 
							foreach ($effects as $key => $value) {
								echo printf('<option value="%s" %s>%s</option>', $key, selected( $instance["effect"], $key ), $value );
							}						
						?>					  
					</select>	
				</p>
			</div>
		</div>
		<hr>
		<p>
			<label for="<?php echo $this->get_field_id('layout'); ?>"><strong><?php _e('Layout'); ?></strong></label>
			
			<div class="clearfix slider-option-widget">
			<?php
				$radios = array(
					'design-1'  => 'Design #1', 
					'design-2'  => 'Design #2', 
					'design-3'  => 'Design #3', 
					'design-4'  => 'Design #4', 
					'design-5'  => 'Design #5', 
					'design-6'  => 'Design #6', 
					'design-7'  => 'Design #7', 
					'design-8'  => 'Design #8', 
					'design-9'  => 'Design #9', 
					'design-10' => 'Design #10'
				);				

				foreach ($radios as $radio1 => $desc ) {
					echo '<div class="design-col ' . $radio1 . '"><span class="input-name"><input type="radio" name="'.$this->get_field_name('layout').'" value="' . $radio1 . '" id="' . $radio1 . '"', $instance["layout"] == $radio1 ? ' checked="true"' : '', '>', $desc, '</input></span></div>';
				}
			?>
			</div>
		</p>
	</div><!-- widget-admin-dev -->			
<?php
	}
}

register_widget( 'Mystyle_Recent_Posts_Slider' );