<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>
<div class="mystyle-post-slide swiper-slide">
	<div class="mystyle-post-content-position">						
		<figure class="mystyle-slide-image">
			<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail('full'); ?>
			</a>
		</figure>
		<div class="mystyle-post-content">
			<h2 class="wp-post-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>

			<time datetime="<?php the_time('Y-m-d')?>">
				<?php the_time('l, F jS, Y') ?>
			</time>	

			<div class="wp-post-content">
				<?php the_excerpt_max_charlength(50); ?>
			</div>
		</div><!-- post-content -->	
	</div><!-- post-content-position -->
</div><!-- post-slides -->