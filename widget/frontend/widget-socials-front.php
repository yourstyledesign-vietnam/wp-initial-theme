<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>

<div class="social-link-container">
	<?php if (ot_get_option('social_facebook')): ?>
		<a target="_blank"  href="<?php echo ot_get_option('social_facebook'); ?>" class="s-link s-facebook"><i class="ion-social-facebook"></i> Facebook</a>
	<?php endif ?>
	
	<?php if (ot_get_option('social_twitter')): ?>
		<a target="_blank"  href="<?php echo ot_get_option('social_twitter'); ?>" class="s-link s-twitter"><i class="ion-social-twitter"></i> Twitter</a>		
	<?php endif ?>	
	
	<?php if (ot_get_option('social_google')): ?>
		<a target="_blank"  href="<?php echo ot_get_option('social_google'); ?>" class="s-link s-email"><i class="ion-social-googleplus"></i> Google+</a>	
	<?php endif ?>			
		
</div><!-- social-link-container -->