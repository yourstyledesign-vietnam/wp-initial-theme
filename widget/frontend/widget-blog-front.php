<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

global $post;
$query = new WP_Query( array(
	'post_type'           => array( 'post' ),
	'showposts'           => $instance['posts_num'],
	'cat'                 => $instance['posts_cat_id'],
	'ignore_sticky_posts' => true,
	'orderby'             => $instance['posts_orderby'],
	'order'               => 'DESC',
	'post__not_in'        => array($post->ID)

) );


if ( $query->have_posts() ) :

?>

<ul class="widget-mystyle-blog">
	<?php while ( $query->have_posts() ): $query->the_post(); ?>
		<li class="clearfix" id="post-<?php the_ID(); ?>">			
			<?php if (has_post_thumbnail('thumbnail')): ?>
				<a href="<?php the_permalink(); ?>" class="figure2" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
				<?php the_post_thumbnail(); ?>
				</a>
			<?php endif; ?>
			
			<div class="widget-blog-desc-holder">					
				<h4 class="post-widget-caption"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
				<?php include(locate_template('component/post-meta.php')) ?>		
			</div>
		</li>
	<?php endwhile; wp_reset_query();?>
</ul>

<?php endif;  ?>