<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

$slider_options = array(
	'layout'            => $instance["layout"],
	'dotsv'             => ( $instance["dotsv"] == 1 ) ? true : false,
	'arrowsv'           => ( $instance["arrowsv"] == 1 ) ? true: false,
	'speedv'            => empty($instance["speedv"])? 400 : $instance["speedv"],
	'autoplayv'         => ( $instance["autoplayv"] == 1 ) ? true : false,
	'autoplayIntervalv' => $instance["autoplayIntervalv"],
	'showposts'         => $instance['posts_num'],
	'posts_cat_id'      => $instance['posts_cat_id'],
	'posts_orderby'     => $instance['posts_orderby'],
	'posts_time'        => $instance['posts_time'],
	'effect'            => $instance['effect'],
	'slidesPerView'     => $instance['slidesPerView'],
	'slidesPerColumn'     => $instance['slidesPerColumn'],

);


$slide_posts = get_posts( array(
	'post_type'				=> array( 'post' ),
	'showposts'				=> $slider_options['showposts'],
	'cat'					=> $slider_options['posts_cat_id'],
	'ignore_sticky_posts'	=> true,
	'orderby'				=> $slider_options['posts_orderby'],
	'order'					=> 'DESC',	
	'meta_query' => array(array('key' => '_thumbnail_id')) 
) );


if ( !empty($slide_posts) ) :

?>
<div class="mystyle-slider-container <?php echo $slider_options['layout']; ?> swiper-container">
	<div class="recent-post-slider swiper-wrapper">
			<?php 
			foreach ( $slide_posts as $post ) : setup_postdata( $post );
				switch ($slider_options['layout']) {
					case "design-1":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-1.php') );
						break;
					case "design-2":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-2.php') );
						break;
					case "design-3":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-3.php') );
						break;
					case "design-4":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-4.php') );
						break;	
					case "design-5":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-5.php') );
						break;
					case "design-6":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-6.php') );
						break;
					case "design-7":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-7.php') );
						break;
					case "design-8":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-8.php') );
						break;
					case "design-9":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-9.php') );
						break;
					case "design-10":
						include( locate_template('widget/frontend/widget-recent-post-slider-design-10.php') );
						break;
				}
			endforeach; 
			wp_reset_postdata();
			?>
		
	</div><!-- recent-post-slider -->
	
	<!-- If we need pagination -->
	<?php if ($slider_options['dotsv']): ?>
		<div class="swiper-pagination"></div>
	<?php endif; ?>
	
	<!-- If we need navigation buttons -->
	<?php if ($slider_options['arrowsv']): ?>
	<div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    <?php endif; ?>

    <!-- If we need scrollbar -->
    <!-- <div class="swiper-scrollbar"></div> -->
	
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		var mySwiper = new Swiper ('.swiper-container', {

			<?php if ($slider_options['arrowsv']): ?>
			nextButton: '.swiper-button-next',
    		prevButton: '.swiper-button-prev',
    		<?php endif; ?>
			   
			<?php if ($slider_options['dotsv']): ?>
			pagination: '.swiper-pagination',
			<?php endif; ?>	

			<?php if ($slider_options['autoplayv']): ?>
			autoplay: <?php echo $slider_options['autoplayIntervalv']; ?>,
			<?php endif; ?>	
			
			speed: 	<?php echo $slider_options['speedv']; ?>,

			effect: "<?php echo $slider_options['effect']; ?>",
			
			slidesPerColumn: "<?php echo $slider_options['slidesPerColumn']; ?>",

			slidesPerView: "<?php echo $slider_options['slidesPerView']; ?>",

	      	loop: true
	    });   
	});
</script>
<?php endif;  ?>