<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>
<?php if (ot_get_option('contact_adrress')): ?>
<div class="widget-contact-text">
	<strong><?php _e("Our Address","mystyle"); ?></strong>
	<p><?php echo ot_get_option('contact_adrress'); ?> </p>
</div>	
<?php endif ?>

<?php if (ot_get_option('contact_phone')): ?>
	<hr>
	<div class="widget-contact-text">
		<strong>
		  <?php _e("Call Us","mystyle"); ?>
		</strong>
		<p><a href="tel:<?php echo ot_get_option('contact_phone'); ?>"><?php echo ot_get_option('contact_phone'); ?></a></p>
	</div>
<?php endif ?>

<?php if (ot_get_option('contact_email')): ?>
	<hr>
	<div class="widget-contact-text">
		<strong>
		  <?php _e("Email Us","mystyle"); ?>
		</strong>
		<p>
		  <a href="mailto:<?php echo ot_get_option('contact_email'); ?>"><?php echo ot_get_option('contact_email'); ?></a>
		</p>
	</div>
<?php endif; ?>

<?php if (ot_get_option('contact_fax')): ?>
	<hr>
	<div class="widget-contact-text">
	<strong>
	  <?php _e("Fax","mystyle"); ?>
	</strong>
	<p>
	  	<?php echo ot_get_option('contact_fax'); ?>
	</p>
	</div>
<?php endif ?>