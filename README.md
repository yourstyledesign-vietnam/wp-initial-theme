wordpress-boilerplate-theme
===========================

Customized by luubinhan1989@gmail.com

# 2016-04-07

* Add recent post slider

# Plugins included in "Theme mode"

* Option Tree 2.6.0

# Library Included

* Swiper: http://idangero.us/swiper
* Bootstrap 3.3.5: http://getbootstrap.com/
* ionicons: http://ionicons.com/
* modernizr: https://modernizr.com/
* jQuery BlockUi : http://malsup.com/jquery/block/

# Recommend Plugins

* Product filter: YITH WooCommerce Ajax Product Filter, https://wordpress.org/plugins/yith-woocommerce-ajax-navigation/
* Front-End Login: Ultimate member, https://wordpress.org/plugins/ultimate-member/


* Nicer select http://silviomoreto.github.io/bootstrap-select/ or https://select2.github.io/examples.html, http://gregfranko.com/jquery.selectBoxIt.js

# BUILD IN FUNCTION
>> remove anonymous action

```php
// remove a static method
remove_object_filter( 'a_filter_hook', 'AClass', 'a_static_method', 10 );

// remove a dynamic method
remove_object_filter( 'a_filter_hook', 'AClass', 'a_dynamic_method', 10 );

// remove a closure
remove_object_filter( 'a_filter_hook', 'Closure', NULL, 10 );
```

>> get_current_user_role() : get user role name

```php
    $user_role = get_current_user_role();
```

>> the_excerpt_max_charlength() : Custom The Excerpt

```php
the_excerpt_max_charlength(25);
```

>> body_page_bg() : get body background, usally set for feature image

```php

$body_bg = body_page_bg();
$style_bg = "";
if ( $body_bg ) {
    $style_bg = sprintf('style="background-image:url(%s);"', $body_bg );
}
```

>> Session Handler

See @class-session-handler.php

```php
Mystyle_Session::get(<name>);
Mystyle_Session::set(<name>,<value>)
Mystyle_Session::remove(<name>)
Mystyle_Session::clear()
```


>> Log

See @log.php

```php
_log(<value>, __METHOD__);
```

## Awsome Ajax Table


1. Add menu to admin

```php
add_action( 'admin_menu', 'mystyle_application_menu' );
/** Step 1. */
function mystyle_application_menu() {
    add_menu_page( __( 'Application', 'keenly' ), __( 'Application', 'keenly' ), 'manage_options', 'keenly-application-menu', array($this,'keenly_application_page_handle'), 'dashicons-welcome-learn-more', 11 );
    add_submenu_page( 'keenly-application-menu', __( 'Ranking check', 'keenly' ), __( 'Ranking check', 'keenly' ), 'manage_options', 'keenly-application-popular-company', array($this,'mystyle_application_popular_handle'));
    
}
```

2. Display table

```php
/**
* Display Popular Company/Keyword Table
* @author An Luu        
*/ 
function mystyle_application_popular_handle() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    $active_tab = 'popular_company';
    $current_month = date('n');    
    $show_month = $current_month;  

    if( isset( $_GET[ 'tab' ] ) ) {
      $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'popular_company';
    } // end if
    if ( isset($_GET['month']) ) {
        $show_month =  stripcslashes($_GET['month']) ;
    }
    wp_register_script('google-chart', 'https://www.gstatic.com/charts/loader.js', '', '', true );
    wp_register_script('keenly-chart', KEENLY_URL . 'includes/js/chart.js', 'google-chart','', true);

    wp_enqueue_script('google-chart');
    wp_enqueue_script('keenly-chart');

    wp_localize_script('keenly-chart', 'ajaxhandle', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' )
    ));
    $today_apply_number = Keenly_Job_Apply_Helper::total_apply_number();
    //$today_apply_number = $today_apply_number[0];
    echo sprintf("<h1>%s</h1>",_x("Ranking Check","Page title","keenly" ));
    echo '<div class="wrap">';
    ?>
    <h2 class="nav-tab-wrapper">
        <a href="?page=keenly-application-popular-company&tab=popular_company" class="nav-tab <?php echo $active_tab == 'popular_company' ? 'nav-tab-active' : ''; ?>"><?php echo _x("Top 10 popular companies","Top 10 popular companies","keenly" ); ?></a>
        <a href="?page=keenly-application-popular-company&tab=popular_keywords" class="nav-tab <?php echo $active_tab == 'popular_keywords' ? 'nav-tab-active' : ''; ?>"><?php echo _x("Top 10 popular search keywords","Top 10 popular search keywords","keenly" ); ?></a>
    </h2>

    <?php
    if ( $active_tab == 'popular_company') {
        echo sprintf("<h1><strong>%s/%s</strong></h1>",$show_month,date('Y'));
        $table = new Awesome_Ajax_Table(
            array(
                'id'        => 'keenly-application-popular-company',
                'columns'   =>
                    array(
                        'rank'         => _x('Rank','Top 10 popular search keywords', 'keenly' ),
                        'last_rank'    => _x('Last Rank','Top 10 popular search keywords','keenly'),
                        'company'      => _x('Company\'s name','Top 10 popular search keywords', 'keenly'),
                        'job'          => _x('Job Article', 'Top 10 popular search keywords','keenly'),                        
                        'hits'         => _x('Hits','Top 10 popular search keywords', 'keenly'),   
                    ),
                'callback'  =>
                    array(
                        'Keenly_Awesome_Table',
                        'get_data_application_popular_admin'
                    ),
                'use_pjax' => false,   
                'extra_args' => array(
                        'month' => $show_month
                    )                           
            )
        );
        $table->display();

   
        ?>
        <ul id="mySubnav" class="subsubsub">
            <li>
                <a href="?page=keenly-application-popular-company&tab=popular_company&month=<?php echo ($show_month == $current_month ) ? $show_month - 1 : $current_month ; ?>" class="current">
                    
                <?php echo ($show_month == $current_month ) ? __("Previous Month","keenly") : __("This Month","keenly"); ?>
                </a>
            </li>
        </ul>
        <br class="clear" />     
        <h3><strong><?php _e("Number of students applied job",'keenly' ); ?></strong></h3>    
        <br />     
        <div class="chart-wrapper">
            <div id="myPieChart" style="width:100%"></div>

        </div>
        <div class="total-display">
            <div class="total-label">
                <?php _e("Total",'keenly' ); ?>
            </div>
            <div class="total-number">
                <?php echo $today_apply_number; ?>
            </div>
        </div>
        <?php

    } elseif ($active_tab == 'popular_keywords') {
        echo sprintf("<h1><strong>%s/%s</strong></h1>",$show_month,date('Y'));
        $table = new Awesome_Ajax_Table(
            array(
                'id'        => 'keenly-popular-keyword',
                'columns'   =>
                    array(
                        'rank'         => _x('Rank','Top popular company','keenly' ),
                        'last_rank'    => _x('Last Rank','Top popular company','keenly'),
                        'keyword'      => _x('Keywork','Top popular company', 'keenly'),                            
                        'hits'         => _x('Hits', 'Top popular company', 'keenly'),   
                    ),
                'callback'  =>
                    array(
                        'Keenly_Awesome_Table',
                        'get_data_keyword_popular_admin'
                    ),
                'use_pjax' => false,   
                'extra_args' => array(
                        'month' => $show_month
                    )                           
            )
        );
        $table->display();
        ?>
        <ul id="mySubnav" class="subsubsub">
            <li>
                <a href="?page=keenly-application-popular-company&tab=popular_keywords&month=<?php echo ($show_month == $current_month ) ? $show_month - 1 : $current_month ; ?>" class="current">
                    
                <?php echo ($show_month == $current_month ) ? __("Previous Month","keenly") : __("This Month","keenly"); ?>
                </a>
            </li>
        </ul>
                  
        
        <?php
    }
    
    echo '</div>';
}
```

3. Get data

```php
/**
* Admin :: Display top 10 popular companies 
* @see /wp-admin/admin.php?page=keenly-application-popular-company
* @author An Luu
*/
static function get_data_application_popular_admin($page, $order_by, $order, $extra_params)
{
    $per_page = apply_filters('awesome_ajax_table_per_page_keenly-application-popular-company', Awesome_Ajax_Table::$per_page);

    $offset = ($page - 1) * $per_page;        
    $order_by = 'rank';
    $order = 'ASC';

    $current_month = date('n');
    if (isset($extra_params['month'])) {
        $current_month = $extra_params['month'];            

    }

    // Get data from keenly_job_apply_rank
    global $wpdb;
    $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . $wpdb->prefix . 'keenly_job_apply_rank             
        WHERE month=' . $current_month . '
        ORDER BY ' . $order_by . ' ' . $order . '
        LIMIT ' . $per_page;

    $results = $wpdb->get_results($sql);
    

    $total_items = intval($wpdb->get_var('SELECT FOUND_ROWS();'));
    $total_items = $per_page;
    $items = array();        

    // Display data
    foreach ($results as $applicant) {        
        $company = get_post($applicant->company_id);

        // GET TOP JOB ARTICLE OF COMPANY
        //SELECT `job_id`, COUNT(*) AS hits FROM `wp_keenly_job_apply` WHERE `company_id`=265 AND DATE(`post_date`) BETWEEN DATE_FORMAT(NOW() ,"%Y-%m-01") AND NOW() GROUP BY `job_id` ORDER BY hits DESC, `post_date` DESC
        $sql_2 = 'SELECT job_id, COUNT(*) AS hits 
            FROM ' . $wpdb->prefix . 'keenly_job_apply 
            WHERE company_id=' . $applicant->company_id . ' AND DATE(post_date) BETWEEN DATE_FORMAT(NOW() ,"%Y-%m-01") AND NOW() 
            GROUP BY job_id 
            ORDER BY hits DESC, post_date DESC
            LIMIT 1';

        $results_2 = $wpdb->get_results($sql_2);
        $job_id    = $results_2[0]->job_id;
        $job       = get_post($job_id);
        
        $items[] = array(
            'rank'         => Keenly_Job_Apply_Helper::table_rank_number($applicant->rank,$applicant->company_id, $current_month),
            'last_rank'    => Keenly_Job_Apply_Helper::table_last_rank($applicant->company_id, $current_month),
            'company'      => Keenly_Job_Apply_Helper::table_title_company($company),
            'job'          => Keenly_Job_Apply_Helper::table_title_job_info_with_hit($job,$results_2[0]->hits),
            'hits'         => Keenly_Job_Apply_Helper::table_simple_number($applicant->number_of_application),
                                
            
        );
       
    }
    return array($items, $total_items);
}
```