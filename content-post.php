<?php  
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for normal post
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>
<?php  
	$class_name = 'mystyle-item-post';	
	$cols_num = ot_get_option('columns_blog');	

	switch ($cols_num) {
		case '1':
			$class_name .= 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
			break;		
		case '2':
			$class_name .= 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
			break;
		case '3':
			$class_name .= 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
			break;
		case '4':
			$class_name .= 'col-xs-12 col-sm-6 col-md-3 col-lg-3';
			break;
	}
?>
<div <?php post_class($class_name) ?> id="post-<?php the_ID(); ?>"> 
	<div class="item-post clearfix">
		<?php $thumbnail_on_off = ot_get_option('thumbnail_on_off'); ?>
		<?php if (has_post_thumbnail() && $thumbnail_on_off == 'on'): ?>
			<figure class="the-post-thumbnail" aria-label="media">
				<a href="<?php the_permalink() ?>">
					<?php the_post_thumbnail( 'thumbnail' ); ?>
				</a>
			</figure>
		<?php endif; ?>
		<section class="the-post-content">
			<?php include(locate_template('component/post-meta.php')) ?>
	  	<header class="heading-post"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></header>	

			<footer class="post-excert" itemprop="description">		
				<?php 
				$max_length = ot_get_option('excerpt_length');
				the_excerpt_max_charlength($max_length); 
				?>
			</footer>
		</section>
	</div><!-- item-post -->
</div>
