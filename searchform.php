<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>
<?php
/**
 * The template for search form
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>
<form method="get" class="navbar-form navbar-left search-box" action="<?php echo home_url( '/' ); ?>" >
	<div class="form-group">
		<input type="text" class="form-control" name="s" id="search-query" value="" placeholder="<?php _e('Search'); ?>" />
		<button type="submit" class="btn btn-default" name="submit"><i class="ion-ios-search-strong"></i>
        </button>	
	</div>  
</form>
