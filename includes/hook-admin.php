<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       anluu.com
 * @since      1.0.0
 *
 * @package    MyStyle
 * @subpackage MyStyle/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    MyStyle
 * @subpackage MyStyle/admin
 * @author     Luu Binh An <an.luu@vn.devinition.com>
 */
class Mystyle_Admin_Hooks
{
	public function __construct() {
		//add_filter( 'show_admin_bar', array($this, 'global_show_hide_admin_bar' ) );
        add_action( 'admin_menu', array($this,'remove_links_menu' ) );
        add_filter( 'enable_post_by_email_configuration', '__return_false' ); // Remove Post Via Email
        add_action( 'admin_head', array($this,'admin_color_scheme'));
        add_action( 'wp_before_admin_bar_render', array($this,'wps_admin_bar' ) );

        /*  Remove deskboard widget
        -------------------------------------------------------------- */
        add_action( 'wp_dashboard_setup', array($this,'mystyle_remove_dashboard_widgets'  ));

        /*  Remove tags from posts listing screen
        -------------------------------------------------------------- */
        add_action( 'manage_posts_columns', array($this,'mystyle_remove_posts_listing_tags' ));

        /*  Remove some column from pages listing screen
        -------------------------------------------------------------- */
        add_filter( 'manage_pages_columns', array($this,'mystyle_custom_pages_columns' ));
        

        /* Admin StyleSheet
        -------------------------------------------------------------- */
        //add_action('admin_head', array($this,'mystyle_admin_head'));


		/*
		 * Disable Theme Updates
		 * 2.8 to 3.0
		 */
		add_filter( 'pre_transient_update_themes', array($this, 'show_update') );
		/*
		 * 3.0
		 */
		add_filter( 'pre_site_transient_update_themes', array($this, 'show_update') );


		/*
		 * Disable Plugin Updates
		 * 2.8 to 3.0
		 */
		add_action( 'pre_transient_update_plugins', array(&$this, 'show_update') );
		/*
		 * 3.0
		 */
		add_filter( 'pre_site_transient_update_plugins', array($this, 'show_update') );


		/*
		 * Disable Core Updates
		 * 2.8 to 3.0
		 */
		add_filter( 'pre_transient_update_core', array($this, 'show_update') );
		/*
		 * 3.0
		 */
		add_filter( 'pre_site_transient_update_core', array($this, 'show_update') );


		/*
		 * Disable All Automatic Updates
		 * 3.7+
		 *
		 * @author	sLa NGjI's @ slangji.wordpress.com
		 */
		add_filter( 'auto_update_translation', '__return_false' );
		add_filter( 'automatic_updater_disabled', '__return_true' );
		add_filter( 'allow_minor_auto_core_updates', '__return_false' );
		add_filter( 'allow_major_auto_core_updates', '__return_false' );
		add_filter( 'allow_dev_auto_core_updates', '__return_false' );
		add_filter( 'auto_update_core', '__return_false' );
		add_filter( 'wp_auto_update_core', '__return_false' );
		add_filter( 'auto_core_update_send_email', '__return_false' );
		add_filter( 'send_core_update_notification_email', '__return_false' );
		add_filter( 'auto_update_plugin', '__return_false' );
		add_filter( 'auto_update_theme', '__return_false' );
		add_filter( 'automatic_updates_send_debug_email', '__return_false' );
		add_filter( 'automatic_updates_is_vcs_checkout', '__return_true' );

		/**
        * Add page 'Application'
        */
        //add_action('admin_init', array($this,'test'));
        //add_action( 'admin_menu', array($this,'mystyle_application_menu') );
	}

    

	function mystyle_application_menu() {
        add_menu_page( 'Application', 'Application', 'manage_options', 'mystyle-application-menu', array($this,'mystyle_application_page_handle'), 'dashicons-welcome-learn-more', 11 );
        add_submenu_page( 'mystyle-application-menu', 'Ranking check', 'Ranking check', 'manage_options', 'mystyle-application-popular-company', array($this,'mystyle_application_popular_handle'));
    }

    /**
    * Display Popular Company/Keyword Table
    * @author An Luu        
    */ 
    function mystyle_application_popular_handle() {
        if ( !current_user_can( 'manage_options' ) )  {
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        }
        $active_tab = 'popular_company';
        if( isset( $_GET[ 'tab' ] ) ) {
          $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'popular_company';
        } // end if
        echo '<div class="wrap">';
        ?>
        <h2 class="nav-tab-wrapper">
            <a href="?page=keenly-application-popular-company&tab=popular_company" class="nav-tab <?php echo $active_tab == 'popular_company' ? 'nav-tab-active' : ''; ?>">Top 10 popular companies</a>
            <a href="?page=keenly-application-popular-company&tab=popular_keywords" class="nav-tab <?php echo $active_tab == 'popular_keywords' ? 'nav-tab-active' : ''; ?>">Top 10 popular search keywords</a>
        </h2>
        <?php
        if ( $active_tab == 'popular_company') {

            $table = new Awesome_Ajax_Table(
                array(
                    'id'        => 'keenly-application-popular-company',
                    'columns'   =>
                        array(
                            'rank'         => __( 'Rank', 'keenly' ),
                            'last_rank'    => __('Last Rank', 'keenly'),
                            'company'      => __('Company\'s name', 'keenly'),
                            'job'          => __('Job Article', 'keenly'),                        
                            'hits'         => __('Hits', 'keenly'),   
                        ),
                    'callback'  =>
                        array(
                            'Keenly_Awesome_Table',
                            'get_data_application_popular_admin'
                        ),
                    'use_pjax' => false,
                    'use_ajax' => false                
                )
            );
            $table->display();
            
        } elseif ($active_tab == 'popular_keywords') {
            # code...
        }
        
        echo '</div>';
    }

    /**
    * Display Application Table
    * @author An Luu        
    */ 
    function keenly_application_page_handle() {
        if ( !current_user_can( 'manage_options' ) )  {
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        }

        add_thickbox();

        echo '<div class="wrap">';
        echo '<h1>Application</h1>';
        $table = new Awesome_Ajax_Table(
            array(
                'id'        => 'keenly-application',
                'columns'   =>
                    array(
                        'applicant'      => __('User\'s name', 'keenly'),
                        'company'        => __('Company\'s name', 'keenly'),
                        'job_url'        => __('Job Url', 'keenly'),
                        'user_status'    => __('User\'s status', 'keenly'),                        
                        'company_status' => __('Company\'s status', 'keenly'),                        
                        'admin_status'   => __('Admin\'s status', 'keenly'),
                    ),
                'callback'  =>
                    array(
                        'Keenly_Awesome_Table',
                        'get_data_application_admin'
                    ),
                'use_pjax' => false,
                'use_ajax' => false
            )
        );
        $table->display();
        echo '</div>';
    }

	function global_show_hide_admin_bar(){

			global $show_admin_bar;

			$theroles = ot_get_option( 'show_admin_on_off' );			

			if ( $theroles == "off" ){
				$show_admin_bar = false;
				return false;
			}		
			if ( current_user_can( 'manage_options' ) && $theroles == "on" ){
				return true;
			}	

	}

	function show_update(){
	  if (ot_get_option('show_update_available') == "off" ) {
	    return true;
	  } else {
	    return false;
	  }
	}
	
    function remove_links_menu() {
        /*remove_menu_page('index.php'); // Dashboard
        remove_menu_page('edit.php'); // Posts
        remove_menu_page('upload.php'); // Media
        remove_menu_page('link-manager.php'); // Links
        remove_menu_page('edit.php?post_type=page'); // Pages
        remove_menu_page('edit-comments.php'); // Comments
        remove_menu_page('themes.php'); // Appearance
        remove_menu_page('plugins.php'); // Plugins
        remove_menu_page('users.php'); // Users
        remove_menu_page('tools.php'); // Tools
        remove_menu_page('options-general.php'); // Settings*/
        remove_menu_page('ot-settings.php'); // Settings*/

        global $submenu;
        unset($submenu['themes.php'][6]); // remove customize link

    }

    /* HIDE ADMIN COLOR SCHEME OPTION FROM USER PROFILE
    -------------------------------------------------------------- */
    function admin_color_scheme() {
       global $_wp_admin_css_colors;
       $_wp_admin_css_colors = 0;
    }

    /* REMOVE WORDPRESS ADMIN BAR
    -------------------------------------------------------------- */

    function wps_admin_bar() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('wp-logo');
        $wp_admin_bar->remove_menu('about');
        $wp_admin_bar->remove_menu('wporg');
        $wp_admin_bar->remove_menu('documentation');
        $wp_admin_bar->remove_menu('support-forums');
        $wp_admin_bar->remove_menu('feedback');
        $wp_admin_bar->remove_menu('view-site');
        $wp_admin_bar->remove_menu('customize');
    }

    
    function mystyle_remove_dashboard_widgets()  {
        $remove_defaults_widgets = array(
            'dashboard_incoming_links' => array(
                'page'    => 'dashboard',
                'context' => 'normal'
            ),
            'dashboard_right_now' => array(
                'page'    => 'dashboard',
                'context' => 'normal'
            ),
            'dashboard_recent_drafts' => array(
                'page'    => 'dashboard',
                'context' => 'side'
            ),
            'dashboard_quick_press' => array(
                'page'    => 'dashboard',
                'context' => 'side'
            ),
            'dashboard_plugins' => array(
                'page'    => 'dashboard',
                'context' => 'normal'
            ),
            'dashboard_primary' => array(
                'page'    => 'dashboard',
                'context' => 'side'
            ),
            'dashboard_secondary' => array(
                'page'    => 'dashboard',
                'context' => 'side'
            )
        );
     
        foreach ($remove_defaults_widgets as $wigdet_id => $options)  {
            remove_meta_box($wigdet_id, $options['page'], $options['context']);
        }
    }

    function mystyle_remove_posts_listing_tags( $columns ) {
        unset( $columns[ 'tags' ] );
        unset( $columns[ 'comments' ] );
        unset( $columns['author'] );
        return $columns;
    }

    function mystyle_custom_pages_columns($columns) {
 
      unset( $columns['author'] );
      unset( $columns[ 'comments' ] );
      unset( $columns[ 'date' ] );
     
      return $columns;
    } 

    function mystyle_unregister_default_widgets() {
        unregister_widget('WP_Widget_Pages');
        unregister_widget('WP_Widget_Calendar');
        unregister_widget('WP_Widget_Archives');
        unregister_widget('WP_Widget_Links');
        unregister_widget('WP_Widget_Categories');
        unregister_widget('WP_Widget_RSS');
        unregister_widget('WP_Widget_Tag_Cloud');
        unregister_widget('WP_Widget_Meta');
        unregister_widget('WP_Widget_Recent_Comments');
        //unregister_widget('WP_Widget_Text');
    }
    function mystyle_admin_head() {
       echo '<link rel="stylesheet" type="text/css" href="' .TEMPLATE_URL.'/css/wp-admin.css'. '">';
    }

}
new Mystyle_Admin_Hooks();