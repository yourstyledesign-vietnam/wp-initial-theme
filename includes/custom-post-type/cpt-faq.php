<?php  
if ( ! function_exists('cpt_faq') ) {

// Register Custom Post Type
function cpt_faq() {

	$labels = array(
		'name'                => _x( 'FAQ', 'Post Type General Name', 'mystyle' ),
		'singular_name'       => _x( 'FAQ', 'Post Type Singular Name', 'mystyle' ),
		'menu_name'           => __( 'FAQ', 'mystyle' ),
		'name_admin_bar'      => __( 'FAQ', 'mystyle' ),
		'parent_item_colon'   => __( 'Parent Item:', 'mystyle' ),
		'all_items'           => __( 'All Items', 'mystyle' ),
		'add_new_item'        => __( 'Add New Item', 'mystyle' ),
		'add_new'             => __( 'Add New', 'mystyle' ),
		'new_item'            => __( 'New Item', 'mystyle' ),
		'edit_item'           => __( 'Edit Item', 'mystyle' ),
		'update_item'         => __( 'Update Item', 'mystyle' ),
		'view_item'           => __( 'View Item', 'mystyle' ),
		'search_items'        => __( 'Search Item', 'mystyle' ),
		'not_found'           => __( 'Not found', 'mystyle' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'mystyle' ),
	);
	$args = array(
		'label'               => __( 'FAQ', 'mystyle' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-format-chat',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'faq', $args );

}
add_action( 'init', 'cpt_faq', 0 );

}
?>