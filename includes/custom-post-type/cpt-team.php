<?php  
if ( ! function_exists('cpt_team') ) {

// Register Custom Post Type
function cpt_team() {

	$labels = array(
		'name'                => _x( 'Our team', 'Post Type General Name', 'mystyle' ),
		'singular_name'       => _x( 'Member', 'Post Type Singular Name', 'mystyle' ),
		'menu_name'           => __( 'Team', 'mystyle' ),
		'name_admin_bar'      => __( 'Team', 'mystyle' ),
		'parent_item_colon'   => __( 'Parent Item:', 'mystyle' ),
		'all_items'           => __( 'All Items', 'mystyle' ),
		'add_new_item'        => __( 'Add New Item', 'mystyle' ),
		'add_new'             => __( 'Add New', 'mystyle' ),
		'new_item'            => __( 'New Item', 'mystyle' ),
		'edit_item'           => __( 'Edit Item', 'mystyle' ),
		'update_item'         => __( 'Update Item', 'mystyle' ),
		'view_item'           => __( 'View Item', 'mystyle' ),
		'search_items'        => __( 'Search Item', 'mystyle' ),
		'not_found'           => __( 'Not found', 'mystyle' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'mystyle' ),
	);
	$args = array(
		'label'               => __( 'Member', 'mystyle' ),
		'description'         => __( 'Custom post type for team', 'mystyle' ),
		'labels'              => $labels,
		'supports'            => array( ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'our-team', $args );

}
add_action( 'init', 'cpt_team', 0 );

}
?>