<?php  
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_template-contact',
		'title' => 'Template :: Contact',
		'fields' => array (
			array (
				'key' => 'field_57ccdc6b9855b',
				'label' => 'Show map',
				'name' => 'show_map',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template/template-contact.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}