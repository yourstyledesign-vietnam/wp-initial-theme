# Section Hero

PHP CODE EXAMPLE

```html
<!-- SWIPER -->
<div class="swiper-relative">
<div class="swiper-container-about">
    <?php  
      if (has_rows('section_hero')):  
    ?>
    <div class="swiper-wrapper">           
        <?php while(has_rows('section_hero')): the_row(); ?>
        <div class="swiper-slide">
          <div class="about-row">
            <?php if (get_sub_field('hero_image')): ?>
              <div class="about-image">
                <img src="<?php the_sub_field('hero_image') ?>" alt="">  
              </div>
            <?php endif; ?>
            
            <div class="about-text">
              <div class="about-inner">
                <?php if (get_sub_field('hero_caption')): ?>
                <h4><?php the_sub_field('hero_caption') ?></h4>  
                <?php endif; ?>
                
                <?php if (get_sub_field('hero_subcaption')): ?>
                <div class="f14">
                  <?php the_sub_field('hero_subcaption') ?>
                </div>
                <?php endif ?>
              </div>
            </div>
          </div>
        </div><!-- swiper-slide-about -->               
        <?php endwhile; ?>
    </div>    <!-- swiper-wrapper-about -->
    <?php endif; ?>

    
</div>
<div class="swiper-pagination"></div>
<div class="swiper-button-next"></div>
<div class="swiper-button-prev"></div>
</div>
<!-- END SWIPER -->
```