```php
<!-- ACF: TEAM SECTION COLUMNS -->
<?php  
    
    $args = array(      
        'post_type'      => 'our-team',
        'post_status'    => 'publish',
        'nopaging'       => true,   
        'posts_per_page' => get_field('number_of_member_display')
    );
    
    $query = new WP_Query( $args );
    if ($query->have_posts()):

?>
<section class="section-acf-team">
    <div class="section-inner">
        <div class="container">
            <?php if (get_field('member_title')): ?>
                <!-- Title -->
                <div class="row">
                    <div class="col-md-12 align-center"> 
                        <h2 class="sc-title">
                        <?php the_field('member_title') ?>
                        </h2>               
                    </div>
                </div>
                <!-- /Title -->
            <?php endif ?>
            

            <div class="row">
                <?php while($query->have_posts()): $query->the_post(); ?>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="member-container match-height">
                        <?php if (get_field('team_avatar')): ?>
                        <div class="member-figure">
                            <img src="<?php the_field('team_avatar') ?>" alt="<?php the_title() ?>">
                        </div>  
                        <?php endif; ?>
                        
                        <div class="p-20">
                            <div class="member-name">
                                <?php the_title(); ?>
                            </div>
                            <?php if (get_field('team_job_title')): ?>
                            <div class="member-position">
                                <?php the_field('team_job_title') ?>
                            </div>      
                            <?php endif ?>                          
                        </div>
                    </div> <!-- member -->                  
                </div> <!-- col -->             
                <?php endwhile; wp_reset_query(); ?>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
</section>
<?php endif; ?>
```
