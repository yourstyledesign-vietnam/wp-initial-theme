<?php
if (!defined('ABSPATH')) {
    die('@@'); // Exit if accessed directly
}

class Keenly_Awesome_Table
{
    static function get_data_company_applicants($page, $order_by, $order, $extra_params)
    {
        $per_page = 10;
        
        $offset = ($page - 1) * $per_page;

        $order_by = 'ID';
        $order = 'DESC';

        // Set extra parameters
        if (!empty($extra_params['action_filter_accepted'])) {
            $statuses = array();
            if ($extra_params['action_filter_accepted'] == "true") {
                $statuses[] = '"hired"';
            }
            if ($extra_params['action_filter_declined'] == "true") {
                $statuses[] = '"non-hired"';
            }
            if ($extra_params['action_filter_no_action'] == "true") {
                $statuses[] = '"new"';
            }

            if (!empty($statuses)) {
                $status = '(' . implode(',', $statuses) . ')';
            } else {
                $status = '%%';
            }
        } else {
            $status = '%%';
        }

        $company = keenly_get_current_company();

        global $wpdb;
        $sql = $wpdb->prepare('
            SELECT SQL_CALC_FOUND_ROWS * FROM ' . $wpdb->prefix . 'keenly_job_apply 
            WHERE ' . (($status != '%%') ? 'status IN ' . $status . ' AND ' : '') . 'company_id = %d
            ORDER BY ' . $order_by . ' ' . $order . ' 
            LIMIT ' . $per_page . ' OFFSET ' . $offset,
            // Below are params
            $company->ID
        );

        $results = $wpdb->get_results($sql);

        $total_items = intval($wpdb->get_var('SELECT FOUND_ROWS();'));
        $items = array();
        $company = keenly_get_current_company();

        foreach ($results as $applicant) {

            $student = get_user_by('id', $applicant->student_id);
            $job = get_post($applicant->job_id);

            if (is_a($student, 'WP_User') && is_a($job, 'WP_Post')) {
                $items[] = array(
                    'applicant'     => Keenly_Company_Helper::table_title_applicant_info($student, $applicant->ID),
                    'job'           => Keenly_Company_Helper::table_title_job_info($job, $company),
                    'created_time'  => Keenly_Company_Helper::table_title_created_time($applicant->post_date),
                    'status'        => Keenly_Company_Helper::table_title_status($applicant->status, $applicant->is_message_sent),
                    'operation'     => Keenly_Company_Helper::table_title_operation($applicant)
                );
            }
        }

        return array($items, $total_items);
    }

    /**
    * Admin :: Display top 10 popular companies 
    * @see /wp-admin/admin.php?page=keenly-application-popular-company
    * @author An Luu
    */
    static function get_data_application_popular_admin($page, $order_by, $order, $extra_params)
    {
        $per_page = apply_filters('awesome_ajax_table_per_page_keenly-application-popular-company', Awesome_Ajax_Table::$per_page);

        $offset = ($page - 1) * $per_page;        
        $order_by = 'rank';
        $order = 'ASC';
        $current_month = date('n');

        // Get data from keenly_job_apply_rank
        global $wpdb;
        $sql = 'SELECT * FROM ' . $wpdb->prefix . 'keenly_job_apply_rank             
            WHERE month=' . $current_month . '
            ORDER BY ' . $order_by . ' ' . $order . '
            LIMIT ' . $per_page;

        $results = $wpdb->get_results($sql);
        

        $total_items = intval($wpdb->get_var('SELECT FOUND_ROWS();'));

        $items = array();        

        // Display data
        foreach ($results as $applicant) {        
            $company = get_post($applicant->company_id);

            // GET TOP JOB ARTICLE OF COMPANY
            //SELECT `job_id`, COUNT(*) AS hits FROM `wp_keenly_job_apply` WHERE `company_id`=265 AND DATE(`post_date`) BETWEEN DATE_FORMAT(NOW() ,"%Y-%m-01") AND NOW() GROUP BY `job_id` ORDER BY hits DESC, `post_date` DESC
            $sql_2 = 'SELECT job_id, COUNT(*) AS hits 
                FROM ' . $wpdb->prefix . 'keenly_job_apply 
                WHERE company_id=' . $applicant->company_id . ' AND DATE(post_date) BETWEEN DATE_FORMAT(NOW() ,"%Y-%m-01") AND NOW() 
                GROUP BY job_id 
                ORDER BY hits DESC, post_date DESC
                LIMIT 1';

            $results_2 = $wpdb->get_results($sql_2);
            $job_id    = $results_2[0]->job_id;
            $job       = get_post($job_id);
            
            $items[] = array(
                'rank'         => Keenly_Job_Apply_Helper::table_simple_number($applicant->rank),
                'last_rank'    => Keenly_Job_Apply_Helper::table_last_rank($applicant->company_id, $current_month),
                'company'      => Keenly_Job_Apply_Helper::table_title_company($company),
                'job'          => Keenly_Job_Apply_Helper::table_title_job_info_with_hit($job,$results_2[0]->hits),
                'hits'         => Keenly_Job_Apply_Helper::table_simple_number($applicant->number_of_application),
                                    
                
            );
           
        }
        return array($items, $total_items);
    }

    /**
    * Admin :: Display List Application
    * @see /wp-admin/admin.php?page=keenly-application-menu
    * @author An Luu
    */
    static function get_data_application_admin($page, $order_by, $order, $extra_params)
    {
        $per_page = apply_filters('awesome_ajax_table_per_page_keenly-application', Awesome_Ajax_Table::$per_page);

        $offset = ($page - 1) * $per_page;        
        $order_by = 'ID';
        $order = 'DESC';

        global $wpdb;
        $sql = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . $wpdb->prefix . 'keenly_job_apply             
            ORDER BY ' . $order_by . ' ' . $order . ' 
            LIMIT ' . $per_page . ' OFFSET ' . $offset;

        $results = $wpdb->get_results($sql);

        $total_items = intval($wpdb->get_var('SELECT FOUND_ROWS();'));

        $items = array();        

        foreach ($results as $applicant) {

            $student = get_user_by('id', $applicant->student_id);
            $job     = get_post($applicant->job_id);
            $company = get_post($applicant->company_id);
            echo $applicant->is_gift_applied;
            if ( $applicant->is_gift_applied == 0 && $applicant->status == 'new') {
              
            } else {
                if (is_a($student, 'WP_User') && is_a($job, 'WP_Post') ) {
                    $items[] = array(
                        'applicant'      => Keenly_Job_Apply_Helper::table_title_applicant($student),
                        'company'        => Keenly_Job_Apply_Helper::table_title_company($company),
                        'job_url'        => Keenly_Job_Apply_Helper::table_title_job_info($job),
                        'user_status'    => Keenly_Job_Apply_Helper::table_student_status($applicant->is_gift_applied), // Applied or empty
                        'company_status' => Keenly_Job_Apply_Helper::table_company_status($applicant->status), // Hired or Non-hired or empty
                        'admin_status'   => Keenly_Job_Apply_Helper::table_admin_status($applicant, $company, $student, $job),
                    );
                }
            }
            
            
        }
        return array($items, $total_items);
    }

    /**
     * Admin :: Display List Of Jobs That Are Liked By Students In Company MyPage
     * @author Minh Dang
     */

    static function get_data_company_job_favorites($page, $order_by, $order, $extra_params)
    {
        $per_page = apply_filters('awesome_ajax_table_per_page_job-favorites', Awesome_Ajax_Table::$per_page);
        $offset = ($page - 1) * $per_page;

        $order_by = 'ID';
        $order = 'DESC';

        $company = keenly_get_current_company();

        global $wpdb;
        $sql = $wpdb->prepare('
            SELECT SQL_CALC_FOUND_ROWS * FROM ' . $wpdb->prefix . Model_Job_Favorite::$table_name . ' f, wp_users u
            WHERE company_id = %d AND u.ID = f.student_id
            ORDER BY f.' . $order_by . ' ' . $order . '
            LIMIT ' . $per_page . ' OFFSET ' . $offset,
            // Below are params
            $company->ID
        );

        $results = $wpdb->get_results($sql);

        $total_items = intval($wpdb->get_var('SELECT FOUND_ROWS();'));
        $items = array();
        $company = keenly_get_current_company();

        foreach ($results as $applicant) {

            $student = get_user_by('id', $applicant->student_id);
            $job = get_post($applicant->job_id);

            if (is_a($student, 'WP_User') && is_a($job, 'WP_Post')) {
                $items[] = array(
                    'applicant'     => Keenly_Company_Helper::table_title_applicant_info($student, $applicant->ID, true),
                    'job'           => Keenly_Company_Helper::table_title_job_info_favorite($job, $company),
                    'created_time'  => Keenly_Company_Helper::table_title_created_time($applicant->created_time),
                    'status'        => Keenly_Company_Helper::table_title_status($applicant->status, $applicant->is_message_sent),
                    'operation'     => Keenly_Company_Helper::table_title_operation_favorite($applicant)
                );
            }
        }

        return array($items, $total_items);
    }
}