<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       anluu.com
 * @since      1.0.0
 *
 * @package    MyStyle
 * @subpackage MyStyle/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    MyStyle
 * @subpackage MyStyle/admin
 * @author     Luu Binh An <an.luu@vn.devinition.com>
*/
class Dev_FrontEnd_Hooks
{
	public function __construct()	{

		/* Remove Empty Paragraph Tags
		-------------------------------------------------------------- */
		add_filter('the_content', array($this,'mystyle_remove_empty_p'), 20, 1);

		/* Logout Redirect to home page
		-------------------------------------------------------------- */
		add_action('wp_logout',array($this,'mystyle_go_home'));

		// Launch operation cleanup
		add_action( 'init', array($this,'mystyle_head_cleanup') );

		add_filter( 'the_generator', array($this,'mystyle_rss_version') );

		
	}
	
	function mystyle_remove_empty_p($content){
	    $content = force_balance_tags($content);
	    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
	}
	
	function mystyle_go_home(){
	  wp_redirect( home_url() );
	  exit();
	}

	function get_current_user_role () {
	    global $current_user;
	    get_currentuserinfo();
	    $user_roles = $current_user->roles;
	    $user_role = array_shift($user_roles);
	    return $user_role;
	}

	/*
	 * Clean up the WordPress Head
	 */

	  function mystyle_head_cleanup() {
	    // remove header links
	    remove_action( 'wp_head', 'feed_links_extra', 3 );                    // Category Feeds
	    remove_action( 'wp_head', 'feed_links', 2 );                          // Post and Comment Feeds
	    remove_action( 'wp_head', 'rsd_link' );                               // EditURI link
	    remove_action( 'wp_head', 'wlwmanifest_link' );                       // Windows Live Writer
	    remove_action( 'wp_head', 'index_rel_link' );                         // index link
	    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );            // previous link
	    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );             // start link
	    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Links for Adjacent Posts
	    remove_action( 'wp_head', 'wp_generator' );                           // WP version
	  }


	/*
	 * remove WP version from RSS
	 */

	function mystyle_rss_version() { return ''; }


	

}
new Dev_FrontEnd_Hooks();