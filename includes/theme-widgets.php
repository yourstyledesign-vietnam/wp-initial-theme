<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>
<?php
/*-----------------------------------------------------------------------------------*/
/* Load the widgets, with support for overriding the widget via a child theme.
/*-----------------------------------------------------------------------------------*/

foreach ( glob(MYSTYLE_DIRECTORY . "widget/admin/widget-*.php") as $file ) {
	include( $file );
}

/*---------------------------------------------------------------------------------*/
/* Deregister Default Widgets */
/*---------------------------------------------------------------------------------*/
if (!function_exists( 'mystyle_deregister_widgets')) {
	function mystyle_deregister_widgets(){
	    //unregister_widget( 'WP_Widget_Search' );         
	}
}
add_action( 'widgets_init', 'mystyle_deregister_widgets' );