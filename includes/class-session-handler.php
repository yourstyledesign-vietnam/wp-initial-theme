<?php
/**
* Short description for class
*
* Mystyle_Session::get
* Mystyle_Session::set
* Mystyle_Session::Remove
*
* @category   Mystyle
* @package    Wordpress
* @subpackage MyStyle
* @copyright  luubinhan1989@gmail.com
* @version    MyStyle 1.1
*/
if (!defined('ABSPATH')) {
    die('@@'); // Exit if accessed directly
}

class Mystyle_Session
{
    static public $_session = 'plugin:Keenly';

    public function __construct()
    {

    }

    static public function clear()
    {
        $_SESSION[self::$_session] = null;
    }

    static public function set($k, $v)
    {
        $_SESSION[self::$_session][$k] = $v;
    }

    static public function get($k = null)
    {
        if (empty($k)) {
            return $_SESSION[self::$_session];
        } else {
            return $_SESSION[self::$_session][$k];
        }
    }

    static public function remove($k = null)
    {
        unset($_SESSION[self::$_session][$k]);
    }

    static public function exec()
    {
        add_action('init', array(__CLASS__, 'session_start'), 0);
        add_action('wp_login', array(__CLASS__, 'session_destroy'));
        add_action('wp_logout', array(__CLASS__, 'session_destroy'));
    }

    static public function session_start()
    {
        if (!session_id()) {
            @session_start();
        }
    }

    static public function session_destroy()
    {
        @session_destroy();
    }

}