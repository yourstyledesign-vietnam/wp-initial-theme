<?php
/**
 * Author: triet.trinh
 * Date: 3/18/16
 * Time: 3:52 PM
 */

define("DEV_LOG", true);
define('DEV_LOG_PATH', WP_CONTENT_DIR . '/uploads/logs');

if (!function_exists('_info')) {
    function _info($msg)
    {
        echo $msg . "\r\n";
    }
}

if (!function_exists('_db')) {
    function _db($var)
    {
        _print($var);
    }
}


if (!function_exists('_print')) {
    function _print($var)
    {
        echo '<pre>';
        print_r($var);
        echo '</pre>';

    }
}

if (!function_exists('_log')) {
    function _log($v, $tag = '', $fileName = null)
    {
        if (empty($fileName)) {
            $fileName = "log_" . date("Ymd") . ".txt";
        }
        $logFilePath = DEV_LOG_PATH . '/' . $fileName;
        $fh = fopen($logFilePath, 'a');
        if (is_array($v) || is_object($v)) {
            ob_start();
            print_r($v);//json_encode($v);
            $v = ob_get_clean();
        }

        $tag = empty($tag) ? '' : $tag . "\t";

        $str = date("Y/m/d H:i:s") . "\t" . $tag . $v . "\r\n\r\n";

        fwrite($fh, $str);
        fclose($fh);
    }
}