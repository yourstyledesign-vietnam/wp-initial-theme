<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php
/*
Template Name: Home
*/
/**
 * The template for Home page
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>
<?php get_header(); ?>

<?php get_footer(); ?>