<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php
/*
Template Name: Contact
*/
/**
 * The template for Contact 7 page
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>
<?php get_header(); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhktXAG6guKrLzHSMjAqPO9ki-SKP556Y" async defer></script>
<script>
    var map;
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'));
    }
</script>
<div class="container">
    <div class="contact-form">
    	<?php if (get_field('show_map')): ?>
    	<section class="section section-map">
            <div class="row">
                <div id="map"></div>
                <div id="marker" data-image="<?php echo TEMPLATE_URL; ?>images/map-marker.png"></div>
                <div class="location-link" data-latitude="10.734663" data-longitude="106.664908" data-address="Chung cu bong sao ta quang buu"></div>
            </div>
        </section>	
    	<?php endif; ?>
        
        <div class="row">
            <div class="col-md-5">
                <div class="box match-height">

                	<!-- ADDRESS -->
                	<?php if (ot_get_option('contact_adrress')): ?>
                		<h3><?php _e("Our Address","mystyle" ); ?></h3>
                    	<?php echo ot_get_option('contact_adrress'); ?>	
                	<?php endif; ?>
    				
    				<!-- PHONE -->
                	<?php if (ot_get_option('contact_phone')): ?>
                		<h3><?php _e("Call Us","mystyle" ); ?></h3>
                		<p>
                    	<a href="tel:<?php echo ot_get_option('contact_phone'); ?>"><?php echo ot_get_option('contact_phone'); ?></a>
                    	</p>
                	<?php endif; ?>
                    
                    <!-- MAIL -->
                    <?php if (ot_get_option('contact_email')): ?>
                		<h3><?php _e("Email Us","mystyle" ); ?></h3>
                		<p>
                    	<a href="mailto:<?php echo ot_get_option('contact_email'); ?>"><?php echo ot_get_option('contact_email'); ?></a>
                    	</p>
                	<?php endif; ?>
                    
                    <!-- FAX -->
                    <?php if (ot_get_option('contact_fax')): ?>
                		<h3><?php _e("Fax","mystyle" ); ?></h3>
                		<p>
                    	<?php echo ot_get_option('contact_fax'); ?>
                    	</p>
                	<?php endif; ?>
                </div>
                <!-- box -->
            </div>
            <div class="col-md-7">
                <div class="box match-height">
                	<?php 
    	            	if (have_posts()) {
    	            		while (have_posts()) {
    	            			the_post();
    	            			the_content();
    	            		}
    	            	} 
                	?>                
                </div>
                <!-- box -->
            </div><!-- col -->
        </div><!-- row -->
    </div><!-- contact-form -->
</div>
<?php get_footer(); ?>

<!-- <div class="form-group">[text* your-name placeholder "Name"]</div>
<div class="form-group">[email* your-email placeholder "Email"]</div>
<div class="form-group">[text* your-subject placeholder "Subjcet"]</div>
<div class="form-group">[textarea* your-message placeholder "Message"]</div>[submit "Send Message"] -->