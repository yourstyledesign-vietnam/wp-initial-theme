<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for general page
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.0
 */
?>
<?php get_header(); ?>
<div class="container">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <article class="single-page-container">	  
    <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">  

      <header class="single-post-heading">
        <?php the_title(); ?>        
      </header>     

      <section class="page-content">
        <?php the_content(); ?>  
      </section> <!-- page-content -->    

    </div><!-- page-container -->
  </article>
  <?php endwhile; endif; ?>   
  
</div>
<?php get_footer(); ?>
