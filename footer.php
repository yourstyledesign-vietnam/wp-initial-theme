<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for footer
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
?>    
    </div> <!-- master-inner -->
  </main> <!-- master -->

  <?php if ( ot_get_option('back_to_top') && ot_get_option('back_to_top_image') ): ?>
  <div class="container relative scroll-top-wrapper">
    <a class="scroll_top" href="#top">
      <i class="ion-android-arrow-dropup-circle"></i>
    </a>  
  </div>  
  <?php endif; ?>

  <!-- FOOTER -->
  <div id="footer">
    <footer class="footer">
      <section class="footer-inner">
        <div class="container">
          <div class="copyright">
            <?php echo ot_get_option('footer_left'); ?>
          </div> <!-- copyright -->
          <div class="credit">
            <?php echo ot_get_option('footer_right'); ?>
          </div> <!-- credit -->
        </div> <!-- container -->
      </section> <!-- / footer-inner -->
    </footer> <!-- / footer -->
  </div>
  <!-- / FOOTER -->

  <?php $google_analytics_id = ot_get_option('google_analytics_id') ?>
  <?php if ($google_analytics_id): ?>
    <script>
      var _gaq=[['_setAccount','<?php echo $google_analytics_id; ?>'],['_trackPageview']];
      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
      g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
      s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
  <?php endif ?>
 
			   
  <?php wp_footer(); ?>

</body>
</html>
