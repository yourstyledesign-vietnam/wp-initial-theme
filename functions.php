<?php
/**
 * Core function of theme
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.1
 */
define('TEMPLATE_URL',get_bloginfo('template_url')."/" );
define('TEMPLATE_RELATIVE_URL',wp_make_link_relative(TEMPLATE_URL) );
define('MYSTYLE_DIRECTORY', dirname(__FILE__)  . '/' );
define('TEMPLATE_NAME', 'mystyle' );

/* Load the theme-specific files, with support for overriding via a child theme.
-------------------------------------------------------------- */

$includes = array(
  'includes/sidebar-init.php',      // Initialize widgetized areas
  'includes/theme-widgets.php',     // Theme widgets
  //'includes/custom-post-type/cpt-team.php',
  //'includes/custom-post-type/cpt-portfolio.php',
  //'includes/custom-post-type/cpt-quote.php',
  'includes/wp-bootstrap-navwalker.php',
  'includes/class-tgm-plugin-activation.php',
  'includes/hook-frontend.php',
  'includes/hook-admin.php',  
  'includes/class-session-handler.php',  
  'includes/mstyle-core-function.php',  
  //'includes/class-mystyle-awesome-table.php',
  //'includes/register-acf-field.php',     // Theme widgets
  //'includes/register-custom-post-type.php',     // Theme widgets
);

foreach ( $includes as $i ) {
  locate_template( $i, true );
}

Mystyle_Session::exec();

/* REMOVE SCREEN OPTION
-------------------------------------------------------------- */

function remove_screen_options(){
    return false;
}
//add_filter('screen_options_show_screen', 'remove_screen_options');

/*  WordPress add class to parent element if has submenu
-------------------------------------------------------------- */

function menu_set_dropdown( $sorted_menu_items, $args ) {
    $last_top = 0;
    foreach ( $sorted_menu_items as $key => $obj ) {
        // it is a top lv item?
        if ( 0 == $obj->menu_item_parent ) {
            // set the key of the parent
            $last_top = $key;
        } else {
            $sorted_menu_items[$last_top]->classes['dropdown'] = 'dropdown';
        }
    }
    return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'menu_set_dropdown', 10, 2 );


/*  Option Tree Plugin
-------------------------------------------------------------- */

add_filter( 'ot_theme_mode', '__return_true' );
require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );

// hide ot docs & settings
add_filter( 'ot_show_pages', '__return_false' ); 

// Create Theme Options without using the UI Builder.
require( trailingslashit( get_template_directory() ) . 'includes/theme-options.php' );


/* TGM Required
-------------------------------------------------------------- */

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

function my_theme_register_required_plugins() {
  /**
   * Array of plugin arrays. Required keys are name and slug.
   * If the source is NOT from the .org repo, then source is also required.
   */
  $plugins = array(
    /** This is an example of how to include a plugin pre-packaged with a theme */
   /* array(
      'name'     => 'TGM Example Plugin', // The plugin name
      'slug'     => 'tgm-example-plugin', // The plugin slug (typically the folder name)
      'source'   => get_stylesheet_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source
      'required' => false,
    ),*/
    array(
      'name' => 'Velvet Blues Update URLs',
      'slug' => 'velvet-blues-update-urls',
    ),
    array(
      'name' => 'Advanced Custom Fields',
      'slug' => 'advanced-custom-fields',
    ),
    /*array(
      'name' => 'Contact Form 7',
      'slug' => 'contact-form-7',
    ),*/
    array(
      'name' => 'Rename wp-login.php',
      'slug' => 'rename-wp-login',
    ),
  );
   
  $theme_text_domain = TEMPLATE_NAME;
  /**
   * Array of configuration settings. Uncomment and amend each line as needed.
   * If you want the default strings to be available under your own theme domain,
   * uncomment the strings and domain.
   * Some of the strings are added into a sprintf, so see the comments at the
   * end of each line for what each argument will be.
   */
  $config = array(
    /*'domain'       => $theme_text_domain,         // Text domain - likely want to be the same as your theme. */
    /*'default_path' => '',                         // Default absolute path to pre-packaged plugins */
    /*'menu'         => 'install-my-theme-plugins', // Menu slug */
    'strings'        => array(
      /*'page_title'             => __( 'Install Required Plugins', $theme_text_domain ), // */
      /*'menu_title'             => __( 'Install Plugins', $theme_text_domain ), // */
      /*'instructions_install'   => __( 'The %1$s plugin is required for this theme. Click on the big blue button below to install and activate %1$s.', $theme_text_domain ), // %1$s = plugin name */
      /*'instructions_activate'  => __( 'The %1$s is installed but currently inactive. Please go to the <a href="%2$s">plugin administration page</a> page to activate it.', $theme_text_domain ), // %1$s = plugin name, %2$s = plugins page URL */
      /*'button'                 => __( 'Install %s Now', $theme_text_domain ), // %1$s = plugin name */
      /*'installing'             => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name */
      /*'oops'                   => __( 'Something went wrong with the plugin API.', $theme_text_domain ), // */
      /*'notice_can_install'     => __( 'This theme requires the %1$s plugin. <a href="%2$s"><strong>Click here to begin the installation process</strong></a>. You may be asked for FTP credentials based on your server setup.', $theme_text_domain ), // %1$s = plugin name, %2$s = TGMPA page URL */
      /*'notice_cannot_install'  => __( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', $theme_text_domain ), // %1$s = plugin name */
      /*'notice_can_activate'    => __( 'This theme requires the %1$s plugin. That plugin is currently inactive, so please go to the <a href="%2$s">plugin administration page</a> to activate it.', $theme_text_domain ), // %1$s = plugin name, %2$s = plugins page URL */
      /*'notice_cannot_activate' => __( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', $theme_text_domain ), // %1$s = plugin name */
      /*'return'                 => __( 'Return to Required Plugins Installer', $theme_text_domain ), // */
    ),
  );
  tgmpa( $plugins, $config );
}

/* Custom Login Screen
-------------------------------------------------------------- */

function my_login_screen(){
    echo '<link rel="stylesheet" type="text/css" href="' .TEMPLATE_URL.'/css/wp-login.css'. '">';
}
add_action('login_enqueue_scripts','my_login_screen');

/* jQuery Enqueue
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
  wp_deregister_script('jquery');
  wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js", false, null);
  wp_enqueue_script('jquery');
}

/* Add theme support
-------------------------------------------------------------- */

add_action('after_setup_theme','mystyle_add_theme_support');
function mystyle_add_theme_support() {
  $nav_menus = array(
      'primary-menu' => __( 'Primary Menu', 'mystyle' ),   
      'toolbar-menu' => __( 'Toolbar Menu', 'mystyle' )
  );  
  foreach ($nav_menus as $key => $value) {
    register_nav_menus(array($key => $value));
  }
  add_theme_support( 'woocommerce' );
  add_theme_support( 'nav-menus' );  
  add_theme_support( 'post-thumbnails' );
  add_theme_support( 'title-tag' );  
  add_theme_support( 'custom-background' ); // wp custom background
  set_post_thumbnail_size( 125, 125, true ); // default thumb size
  load_theme_textdomain(TEMPLATE_NAME, get_template_directory() . '/languages');
}


/* Echo wrapper for blog post
-------------------------------------------------------------- */

function the_open_wrapper() {
  echo "<div class='posts-list'>";
}
add_action('before_blog_post','the_open_wrapper' );

function the_close_wrapper() {
  echo "</div>";
}
add_action('after_blog_post','the_close_wrapper' );

/* GET CURRENT USER ROLE
-------------------------------------------------------------- */
add_filter( 'login_head', 'mystyle_custom_login_background' );

// Login Background
function mystyle_custom_login_background() {

  // Background
  $url_bg = ot_get_option('background_login');
  if ($url_bg) {
    echo sprintf('<style type="text/css">body.login {background-image:url(%s);}</style>', $url_bg);
  }
  // Logo
  $url_logo = ot_get_option('logo_login');
  if ($url_logo) {
    echo sprintf('<style type="text/css">body.login h1 {background-image:url(%s);}</style>', $url_logo); 
  }
  
  // Logo
  $url_color = ot_get_option('login_colorpicker');
  if ($url_color) {
    echo sprintf('<style type="text/css">body.login{background-color:%s;}</style>', $url_color); 
  }
}

/* Register global styles & scripts.
-------------------------------------------------------------- */

add_action('wp_enqueue_scripts', 'mystyle_enqueue_scripts');
function mystyle_enqueue_scripts()
{ 
  
  $css = array(
    'mystyle-bootstrap'  => 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css',
    'mystyle-ionicons'   => 'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',    
    'style'              => TEMPLATE_URL . 'style.css',
    'mystyle-responsive' => TEMPLATE_URL . 'style-responsive.css'
  );

  $scripts = array(
      'mystyle-modernizr' => 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js',
      'mystyle-bootstrap' => 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js',      
      'mystyle-plugin'    => TEMPLATE_URL . 'js/plugin.js',
      'mystyle-common'      => TEMPLATE_URL . 'js/common.js'
  );

  foreach ($css as $key => $value) {
    wp_register_style( $key, $value );
  }
  foreach ($scripts as $key => $value) {
    wp_register_script( $key, $value, array('jquery'), null, true );
  }
  foreach ($css as $key => $value) {
    wp_enqueue_style($key);
  }
  foreach ($scripts as $key => $value) {
    wp_enqueue_script($key);
  }
  // Load google map on page contact
  
  
}

/* body background
-------------------------------------------------------------- */
function body_page_bg() {
  global $post;
  return wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') );
}

add_editor_style('editor-style.css');