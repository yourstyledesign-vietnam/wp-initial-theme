<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>

<div class="card-block" id="post-<?php the_ID(); ?>">
  <article <?php post_class() ?>>
    <div class="inner">
      <!-- <div class="card-figure">
        <a href=""><img src="http://placehold.it/400x600" alt="" /></a>
      </div> -->
      <div class="card-header">
        <div class="list-share-circles">
          <ul>
            <li>
              <div class="bt-circle bt-facebook popup">
                <a target="_blank"  href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&ptitle=<?php echo get_the_title() ; ?>">
                <i class="ion-social-facebook"></i>
                </a>
              </div>
            </li>
            <li>
              <div class="bt-circle bt-twitter popup">
                <a target="_blank"  href="http://twitter.com/share?text=<?php echo get_the_title() ; ?>&url=<?php the_permalink(); ?>">
                  <i class="ion-social-twitter"></i>
                </a>
              </div>
            </li>
            <li>
              <div class="bt-circle bt-email popup">
                <a target="_blank"  href="mailto:?subject=<?php echo get_the_title() ; ?>&amp;body=Check out this site <?php the_permalink(); ?>">
                  <i class="ion-android-mail"></i>
                </a>
              </div>
            </li>
          </ul>
          <div class="bt-circle bt-share js-social">
            <i class="ion-android-share-alt"></i>
          </div>
        </div>
      </div>
      <div class="card-body">
        <header class="card-title">
          <h3>
            <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
          </h3>
        </header>      
        <div class="card-desc">
          <?php 
          $max_length = ot_get_option('excerpt_length');
          the_excerpt_max_charlength($max_length); 
          ?>
        </div>
      </div>
      <footer class="card-footer clearfix">
        <div class="grid-50">
          <?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?>
        </div>
        <div class="grid-50">
          <?php the_category(' | ') ?>
        </div>
      </footer>
    </div>
  </article>
</div>