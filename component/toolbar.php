<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * Toolbar
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.0
 */
?>
<!-- TOOLBAR -->
<div id="toolbar">
<nav class="toolbar" >
	<div class="toolbar-inner">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-12 col-lg-8">
					<?php  
					
						$args = array(
		                  'theme_location' => 'toolbar-menu',
		                  'container'      => false,
		                  'menu'           => 'toolbar-menu',                      
		                  'depth'          => 2,
		                  'menu_class'     => 'top-nav',
		                  'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
		                  'walker'         => new wp_bootstrap_navwalker()
		                );
		                wp_nav_menu( $args );
					?>					
				</div>
				<div class="col-md-6 col-xs-12 col-lg-4">
					<div class="topnote">
						 <?php echo ot_get_option('toolbar_text'); ?>
					</div>
				</div>
			</div> <!-- row -->
		</div> <!-- container -->
	</div> <!-- toolbar-inner -->
</nav> <!-- toolbar -->
</div>
<!-- / TOOLBAR -->