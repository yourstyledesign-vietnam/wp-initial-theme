<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template of share
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.0
 */
?>
<div class="share-social-component">
	<div class="ssc-label">
		<?php _e("Share this","mystyle"); ?>
	</div>
	<div class="ssc-buttons">
		<div class="social-link-container">
			<a target="_blank"  href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&ptitle=<?php echo get_the_title() ; ?>" class="s-link s-facebook"><i class="ion-social-facebook"></i> Facebook</a>
			<a target="_blank"  href="http://twitter.com/share?text=<?php echo get_the_title() ; ?>&url=<?php the_permalink(); ?>" class="s-link s-twitter"><i class="ion-social-twitter"></i> Twitter</a>			
			<a target="_blank"  href="mailto:?subject=<?php echo get_the_title() ; ?>&amp;body=Check out this site <?php the_permalink(); ?>" class="s-link s-email"><i class="ion-android-mail"></i> Email</a>
        </div><!-- social-link-container -->
	</div><!-- ssc-buttons -->
</div> <!-- share-social-component -->
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php the_title(); ?>">
<meta itemprop="description" content="<?php echo esc_html(get_the_excerpt()); ?>">
<meta itemprop="image" content="<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_id(), 'full') ); ?>">


<!-- Twitter Card data -->
<meta name="twitter:title" content="<?php the_title(); ?>">
<meta name="twitter:description" content="<?php echo esc_html(get_the_excerpt()); ?>">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_id(), 'full') ); ?>">

<!-- Open Graph data -->
<meta property="og:title" content="<?php the_title(); ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php the_permalink(); ?>" />
<meta property="og:image" content="<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_id(), 'full') ); ?>" />
<meta property="og:description" content="<?php echo esc_html(get_the_excerpt()); ?>" />