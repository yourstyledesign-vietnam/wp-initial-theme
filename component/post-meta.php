<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>
<aside class="post-meta">
	<?php $date_format = get_option('date_format'); ?>
  <div class="post-date">
	 <time class="dateline" datetime="<?php the_time('Y-m-d')?>" itemprop="dateModified" content="<?php the_time($date_format); ?>">
		<?php the_time($date_format); ?>
	 </time>			
  </div>
  <div class="post-category">
	<?php _e('in','mystyle' ); ?> <?php the_category(' | ') ?>
  </div><!-- post-category -->
</aside>