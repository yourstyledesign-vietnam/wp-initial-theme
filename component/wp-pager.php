<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
/**
 * The template for pager
 *
 * @package WordPress
 * @subpackage MyStyle
 * @since MyStyle 1.0
 */
?>
<nav aria-label="page-navigation">
  <ul class="pager">
  	<?php if (is_single()): ?>
	    <li>
	    	<?php next_post_link('%link'); ?>
	    </li>
	    <li>	      
	      <?php previous_post_link('%link'); ?>
	    </li>
	<?php else: ?>
		<li>
	    	<?php next_posts_link('&laquo; Older Entries') ?>
	    </li>
	    <li>
	      <?php previous_posts_link('Newer Entries &raquo;') ?>	      
	    </li>
	<?php endif; ?>
  </ul>
</nav>