<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
?>
<div class="read-next">
  <?php
  $next_post = get_next_post();
  
  if (!empty( $next_post )): ?>
    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($next_post->ID, 'full')); ?>
  <a class="read-next-story" href="<?php echo get_permalink( $next_post->ID ); ?>" style="background-image:url('<?php echo $url; ?>');">
    <section class="post">
      <span class="read-this-next"><?php _e("Read this next" ); ?></span>
      <h2><?php echo $next_post->post_title; ?></h2>
    </section>
  </a>
  <?php endif; ?>

  <?php
    $prev_post = get_previous_post();
    if (!empty( $prev_post )): ?>
    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($prev_post->ID, 'full')); ?>
    <a class="read-next-story prev" href="<?php echo get_permalink($prev_post->ID) ?>" style="background-image:url('<?php echo $url; ?>');">
      <section class="post">
        <span class="you-might-enjoy"><?php _e("You might enjoy" ); ?></span>
        <h2><?php echo $prev_post->post_title ?></h2>
      </section>
    </a>
  <?php endif ?>
</div><!-- read-next --> 