<aside class="post-meta muted">
	<time datetime="<?php the_time('Y-m-d')?>">
		<?php the_time('l, F jS, Y') ?>
	</time>			
	at <time><?php the_time() ?></time>
	, <?php the_category(' | ') ?>
</aside>
