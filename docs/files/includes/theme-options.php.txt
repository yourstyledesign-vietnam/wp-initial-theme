<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet */
  if ( ! function_exists( 'ot_settings_id' ) )
    return false;

  $nav_menus = array(
      'primary-menu' => __( 'Primary Menu', 'mystyle' ),
      'home-menu'    => __( 'Home Menu', 'mystyle' ) ,
      'toolbar-menu' => __( 'Toolbar Menu', 'mystyle' )
  );
  $menu_choices = array(
          array(
            'value'       => '',
            'label'       => __( '-- Choose One --', 'mystyle' ),            
          ),
  );
  foreach ($nav_menus as $key => $value) {
    register_nav_menus(array($key => $value));
    $temp = array(
      'value'       => $key,
      'label'       => $value,            
    );
    array_push($menu_choices, $temp);
  }
  /*update_option('thumbnail_size_w', 170);
  update_option('thumbnail_size_h', 170);
  update_option('medium_size_w', 768);
  update_option('medium_size_h', 576);
  update_option('large_size_w', 1020);
  update_option('large_size_h', 768);*/

  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 
             
        array(
            'id'          => 'section_header',
            'title'       => __( 'Header','mystyle'  )
        ),
        array(
            'id'          => 'section_footer',
            'title'       => __( 'Footer','mystyle'  )
        ), 
        array(
            'id'          => 'section_contact',
            'title'       => __( 'Contact Info','mystyle'  )
        ),
        array(
            'id'          => 'section_blog',
            'title'       => __( 'Blog','mystyle'  )
        ),
        array(
            'id'          => 'section_404',
            'title'       => __( '404','mystyle'  )
        ),
        array(
            'id'          => 'section_social',
            'title'       => __( 'Social Links','mystyle'  )
        ),
        array(
            'id'          => 'section_advanced',
            'title'       => __( 'Advanced','mystyle'  )
        ), 
        array(
            'id'          => 'section_others',
            'title'       => __( 'Others','mystyle'  )
        ),
      
    ),
    'settings'        => array( 
      // ====== Header ======
      array(
        'id'          => 'logo_type',
        'label'       => __( 'Logo type', 'mystyle' ),
        'desc'        => __( '', 'mystyle' ),
        'std'         => '',
        'type'        => 'radio',
        'section'     => 'section_header',        
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'text',
            'label'       => __( 'Text', 'mystyle' ),
            'src'         => ''
          ),
          array(
            'value'       => 'image',
            'label'       => __( 'Image', 'mystyle' ),
            'src'         => ''
          ),
          array(
            'value'       => 'text_image',
            'label'       => __( 'Text & Image', 'mystyle' ),
            'src'         => ''
          )
        )
      ),
      array(
        'id'          => 'logo_image',
        'label'       => __( 'Logo Image','mystyle'  ),
        'desc'        => sprintf(''),
        'type'        => 'upload',
        'section'     => 'section_header',
        'operator'    => 'or',
        'condition' => 'logo_type:is(image),logo_type:is(text_image)'
      ),   
      array(
        'id'          => 'logo_text',
        'label'       => __( 'Text logo','mystyle'  ),        
        'type'        => 'text',
        'condition'   => 'logo_type:is(text),logo_type:is(text_image)',
        'section'     => 'section_header',
        'operator'    => 'or'
      ), 
      array(
        'id'          => 'toolbar_on_off',
        'label'       => __( 'Display toolbar menu','mystyle' ),
        'desc'        => __( '' ) ,
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'section_header',     
        'operator'    => 'and'
      ),      
      array(
        'id'          => 'toolbar_text',
        'label'       => __( 'Text on toolbar','mystyle'),        
        'type'        => 'text',
        'condition'   => 'toolbar_on_off:is(on)',
        'section'     => 'section_header',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'search_on_off',
        'label'       => __( 'Display search box','mystyle' ),
        'desc'        => __( '' ) ,
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'section_header',     
        'operator'    => 'and'
      ),
      
         
      // ====== CONTACT ======
      array(
        'id'          => 'contact_adrress',
        'label'       => __( 'Adrress','mystyle'),        
        'type'        => 'textarea',
        'section'     => 'section_contact',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'contact_email',
        'label'       => __( 'Email','mystyle'),        
        'type'        => 'text',
        'section'     => 'section_contact',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'contact_phone',
        'label'       => __( 'Phone','mystyle'),        
        'type'        => 'text',
        'section'     => 'section_contact',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'contact_fax',
        'label'       => __( 'Fax','mystyle'),        
        'type'        => 'text',
        'section'     => 'section_contact',
        'operator'    => 'and'
      ),


      // ====== BLOG ====== 

      array(
        'id'          => 'thumbnail_on_off',
        'label'       => __( 'Thumbnail Preview','mystyle'),
        'desc'        => __( 'Display thumbnail preview for posts','mystyle' ) ,
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'section_blog',     
        'operator'    => 'and'
      ),     
      array(
        'id'          => 'columns_blog',
        'label'       => __( 'Columns','mystyle' ),
        'desc'        => __( 'Set how many columns to display','mystyle' ),     
        'std'         => '1',    
        'type'        => 'numeric-slider',
        'section'     => 'section_blog',        
        'min_max_step'=> '1,4,1',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'excerpt_length',
        'label'       => __( 'Excerpt Length','mystyle' ),
        'desc'        => __( 'Set the excerpt length','mystyle' ),    
        'std'         => '150',    
        'type'        => 'numeric-slider',
        'section'     => 'section_blog',        
        'min_max_step'=> '100,400,20',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'show_blog_header',
        'label'       => __( 'Show blog header','mystyle' ),
        'desc'        => __( 'Display header for blog page','mystyle' ) ,
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'section_blog',     
        'operator'    => 'and'
      ), 
      array(
        'id'          => 'blog_title',
        'label'       => __( 'Blog title','mystyle'),        
        'type'        => 'text',
        'section'     => 'section_blog',
        'condition'   => 'show_blog_header:is(on)',
        'operator'    => 'and'
      ),
      
      array(
        'id'          => 'blog_sub_title',
        'label'       => __( 'Blog sub title','mystyle'),        
        'type'        => 'text',
        'section'     => 'section_blog',
        'condition'   => 'show_blog_header:is(on)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_readmore_link',
        'label'       => __( 'Blog read more link','mystyle'),        
        'type'        => 'text',
        'section'     => 'section_blog',        
        'operator'    => 'and'
      ),    
      array(
        'id'          => 'single_blog',
        'label'       => __( 'Blog Single Page', 'mystyle' ),
        'desc'        => __( 'This section allows you to set up how your single post will look.','mystyle' ),
        'std'         => '',
        'type'        => 'textblock-titled',
        'section'     => 'section_blog',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'single_blog_categories_on_off',
        'label'       => __( 'Display categories','mystyle'),
        'desc'        => __( '' ) ,
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'section_blog',     
        'operator'    => 'and'
      ),
      array(
        'id'          => 'single_blog_tags_on_off',
        'label'       => __( 'Display tags',"mystyle" ),
        'desc'        => __( '' ) ,
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'section_blog',     
        'operator'    => 'and'
      ),
      array(
        'id'          => 'single_blog_related_on_off',
        'label'       => __( 'Show related posts',"mystyle" ),
        'desc'        => __( '' ) ,
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'section_blog',     
        'operator'    => 'and'
      ),
      array(
        'id'          => 'single_blog_related_number',
        'label'       => __( 'Number of related posts', 'mystyle' ),
        'desc'        => __( '' ),
        'std'         => '',
        'type'        => 'numeric-slider',
        'section'     => 'section_blog',
        'min_max_step'=> '3,8,1',        
        'condition'   => 'single_blog_related_on_off:is(on)',
        'operator'    => 'and'
      ),
      
      // ====== 404 ====== 
      array(
        'id'          => 'page_404_options',
        'label'       => __( '404 Page Options', 'mystyle' ),
        'desc'        => __( 'If someone goes to a invalid url this 404 page will be shown. You can change the image, title, text and colour of the 404 page here.','mystyle' ),
        'std'         => '',
        'type'        => 'textblock-titled',
        'section'     => 'section_404',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'page404_image',
        'label'       => __( '404 image',"mystyle"  ),
        'desc'        => sprintf(''),
        'type'        => 'upload',
        'section'     => 'section_404',
        'operator'    => 'and',        
      ), 
      array(
        'id'          => 'page404_title',
        'label'       => __( 'Page Title',"mystyle"  ),        
        'type'        => 'text',
        'std'         => '404. Whoops!',
        'section'     => 'section_404',        
        'operator'    => 'and'
      ),
      array(
        'id'          => 'page404_content',
        'label'       => __( 'Page Text',"mystyle"  ),  
        'std'         => 'We couldn\'t find the page you <br> were looking for',   
        'type'        => 'textarea',
        'section'     => 'section_404',        
        'operator'    => 'and'
      ), 

      // ====== FOOTER ======
      array(
        'id'          => 'footer_left',
        'label'       => __( 'Text on left side','mystyle'),     
        'type'        => 'textarea',
        'section'     => 'section_footer',        
        'operator'    => 'and'
      ),   
      array(
        'id'          => 'footer_right',
        'label'       => __( 'Text on right side','mystyle'),     
        'type'        => 'textarea',
        'section'     => 'section_footer',        
        'operator'    => 'and'
      ), 
      array(
        'id'          => 'back_to_top',
        'label'       => __( 'Enable Back to top button','mystyle'),
        'desc'        => __( '' ) ,
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'section_footer',     
        'operator'    => 'and'
      ),  
      array(
        'id'          => 'back_to_top_image',
        'label'       => __( 'Back to top Image','mystyle'  ),
        'desc'        => sprintf(''),
        'type'        => 'upload',
        'section'     => 'section_footer',
        'operator'    => 'or',
        'condition' => 'back_to_top:is(on)'
      ),
      
      // ====== SOCIAL ======        
      array(
        'id'          => 'social_facebook',
        'label'       => __( 'Facebook' ),        
        'type'        => 'text',
        'section'     => 'section_social',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'social_twitter',
        'label'       => __( 'Twitter' ),        
        'type'        => 'text',
        'section'     => 'section_social',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'social_google',
        'label'       => __( 'Google+' ),        
        'type'        => 'text',
        'section'     => 'section_social',
        'operator'    => 'and'
      ),
      // ====== Advanced ======
      array(
        'id'          => 'google_analytics_id',
        'label'       => 'Google Analytics ID',
        'desc'        => 'asynchronous google analytics: mathiasbynens.be/notes/async-analytics-snippet change the UA-XXXXX-X to be your site\'s ID',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'section_advanced',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'logo_google_fonts',
        'label'       => __( 'Fonts Family', 'mystyle' ),
        'desc'        => '',
        'std'         => array( 
          array(
            'family'    => 'opensans',
            'variants'  => array( '300', '300italic', 'regular', 'italic', '600', '600italic' ),
            'subsets'   => array( 'latin' )
          )
        ),
        'type'        => 'google-fonts',
        'section'     => 'section_advanced',        
        'condition'   => 'logo_type:is(text),logo_type:is(text_image)',
        'operator'    => 'or'
      ),
      array(
        'id'          => 'site_favicon',
        'label'       => __( 'Fav Icon'  ),
        'desc'        => sprintf('The site Fav Icon is the icon that appears in the top corner of the browser tab, it is also used when saving bookmarks. Upload your own custom Fav Icon here, recommended resolutions are 16x16 or 32x32.',"mystyle"),
        'type'        => 'upload',
        'section'     => 'section_advanced',
      ),  
      array(
        'id'          => 'iphone_icon',
        'label'       => __( 'iPhone Icon (57x57)',"mystyle"),
        'desc'        => sprintf('If someone saves a bookmark to their desktop on an Apple device this is the icon that will be used. Here you can upload the icon you would like to be used on the various Apple devices.',"mystyle"),
        'type'        => 'upload',
        'section'     => 'section_advanced',
      ), 
       array(
        'id'          => 'ipad_icon',
        'label'       => __( 'iPad Icon (72x72)',"mystyle"),
        'desc'        => sprintf(''),
        'type'        => 'upload',
        'section'     => 'section_advanced',
      ),    
      array(
        'id'          => 'iphone_icon_retina',
        'label'       => __( 'iPhone Retina Icon (114x114)',"mystyle"),
        'desc'        => sprintf(''),
        'type'        => 'upload',
        'section'     => 'section_advanced',
      ),  
     
      // ====== Others ======  
      array(
        'id'          => 'logo_login',
        'label'       => __( 'Logo login form','mystyle'),
        'desc'        => sprintf('Logo display at login page','mystyle'),
        'type'        => 'upload',
        'section'     => 'section_others',
        'operator'    => 'and'
      ),  
      array(
        'id'          => 'background_login',
        'label'       => __( 'Background image login form','mystyle'),
        'desc'        => sprintf('Background for login page','mystyle'),
        'type'        => 'upload',
        'section'     => 'section_others',
        'operator'    => 'and'
      ),  
      array(
        'id'          => 'login_colorpicker',
        'label'       => __( 'Background color login form','mystyle'),
        'desc'        => sprintf('Background for login page'),
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'section_others',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'show_admin_on_off',
        'label'       => __( 'Show admin bar','mystyle'),
        'desc'        => '',
        'std'         => '',
        'type'        => 'on-off',
        'section'     => 'section_others',   
        'operator'    => 'and'
      ),
      array(
        'id'          => 'show_update_available',
        'label'       => __( 'Show update available','mystyle'),
        'desc'        => '',
        'std'         => '',
        'type'        => 'on-off',
        'section'     => 'section_others',   
        'operator'    => 'and'
      ),

      
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}
